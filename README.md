<h1>FYP</h1>
This is my final year project for University of Surrey, Computer Science
The application is divided in two parts: frontend and backend
<h2>Frontend</h2>
The frontend is made with vue.js. To run the project in development mode clone the git repo and do:

<code>cd fyp_frontend</code>

<code>pnpm install</code>

<code>pnpm dev</code>


<h2>Backend</h2>
The backend is made using Express. To run the project in development mode clone the git repo and do:

<code>cd fyp_backend</code>

<code>pnpm install</code>

<code>pnpm dev</code>
