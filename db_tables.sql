--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

-- Started on 2023-05-14 20:24:37 BST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16414)
-- Name: projects; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA projects;


ALTER SCHEMA projects OWNER TO postgres;

--
-- TOC entry 7 (class 2615 OID 16536)
-- Name: settings; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA settings;


ALTER SCHEMA settings OWNER TO postgres;

--
-- TOC entry 5 (class 2615 OID 16399)
-- Name: users; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA users;


ALTER SCHEMA users OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 219 (class 1259 OID 16416)
-- Name: projects; Type: TABLE; Schema: projects; Owner: postgres
--

CREATE TABLE projects.projects (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    dev_user integer NOT NULL
);


ALTER TABLE projects.projects OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16415)
-- Name: Projects_id_seq; Type: SEQUENCE; Schema: projects; Owner: postgres
--

ALTER TABLE projects.projects ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME projects."Projects_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 229 (class 1259 OID 16568)
-- Name: password_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.password_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    regex character varying(100) DEFAULT '.*'::character varying NOT NULL,
    min_length integer DEFAULT 5 NOT NULL,
    max_length integer DEFAULT 20 NOT NULL,
    hash_length integer DEFAULT 16 NOT NULL
);


ALTER TABLE settings.password_settings OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16567)
-- Name: password_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.password_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.password_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 227 (class 1259 OID 16554)
-- Name: refresh_token_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.refresh_token_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    length integer DEFAULT 16 NOT NULL,
    default_duration integer DEFAULT 14 NOT NULL,
    max_duration integer DEFAULT 32 NOT NULL,
    min_duration integer DEFAULT 7 NOT NULL
);


ALTER TABLE settings.refresh_token_settings OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16553)
-- Name: refresh_token_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.refresh_token_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.refresh_token_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 233 (class 1259 OID 24607)
-- Name: security_questions_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.security_questions_settings (
    id integer NOT NULL,
    n_questions integer DEFAULT 1 NOT NULL,
    project_id integer NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    min_n_questions integer DEFAULT 1 NOT NULL,
    max_n_questions integer DEFAULT 5 NOT NULL
);


ALTER TABLE settings.security_questions_settings OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 24606)
-- Name: security_questions_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.security_questions_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.security_questions_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 225 (class 1259 OID 16538)
-- Name: session_token_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.session_token_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    length integer DEFAULT 16 NOT NULL,
    default_duration integer DEFAULT 14 NOT NULL,
    max_duration integer DEFAULT 28 NOT NULL,
    min_duration integer DEFAULT 1 NOT NULL
);


ALTER TABLE settings.session_token_settings OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16537)
-- Name: session_token_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.session_token_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.session_token_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 231 (class 1259 OID 16583)
-- Name: username_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.username_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    regex character varying(100) DEFAULT '.*'::character varying NOT NULL,
    min_length integer DEFAULT 5 NOT NULL,
    max_length integer DEFAULT 30 NOT NULL
);


ALTER TABLE settings.username_settings OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16582)
-- Name: username_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.username_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.username_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 16401)
-- Name: dev_users; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.dev_users (
    id integer NOT NULL,
    email character varying(50) NOT NULL,
    token character varying(255),
    hash character varying(255)
);


ALTER TABLE users.dev_users OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16400)
-- Name: dev_users_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.dev_users ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME users.dev_users_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 1000
    CACHE 1
    CYCLE
);


--
-- TOC entry 222 (class 1259 OID 16473)
-- Name: end_user_settings; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.end_user_settings (
    id integer NOT NULL,
    end_user_id integer NOT NULL,
    session_token_duration integer NOT NULL,
    refresh_token_duration integer NOT NULL,
    use_security_questions boolean DEFAULT false NOT NULL,
    n_security_questions integer DEFAULT 1 NOT NULL
);


ALTER TABLE users.end_user_settings OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16490)
-- Name: end_user_settings_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.end_user_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.end_user_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 221 (class 1259 OID 16427)
-- Name: end_users; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.end_users (
    id integer NOT NULL,
    project_id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(500) NOT NULL,
    token character varying(256),
    token_expires timestamp without time zone,
    refresh_token character varying(500),
    refresh_token_expires timestamp without time zone
);


ALTER TABLE users.end_users OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16426)
-- Name: end_users_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.end_users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.end_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 235 (class 1259 OID 24622)
-- Name: security_questions; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.security_questions (
    id integer NOT NULL,
    question character varying(100) NOT NULL,
    answer character varying(100) NOT NULL,
    end_user_id integer
);


ALTER TABLE users.security_questions OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 24621)
-- Name: security_questions_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.security_questions ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.security_questions_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3515 (class 2606 OID 16420)
-- Name: projects Projects_pkey; Type: CONSTRAINT; Schema: projects; Owner: postgres
--

ALTER TABLE ONLY projects.projects
    ADD CONSTRAINT "Projects_pkey" PRIMARY KEY (id);


--
-- TOC entry 3525 (class 2606 OID 16575)
-- Name: password_settings password_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.password_settings
    ADD CONSTRAINT password_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3523 (class 2606 OID 16664)
-- Name: refresh_token_settings refresh_token_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.refresh_token_settings
    ADD CONSTRAINT refresh_token_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3529 (class 2606 OID 24611)
-- Name: security_questions_settings security_questions_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.security_questions_settings
    ADD CONSTRAINT security_questions_pkey PRIMARY KEY (id);


--
-- TOC entry 3521 (class 2606 OID 16547)
-- Name: session_token_settings session_token_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.session_token_settings
    ADD CONSTRAINT session_token_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3527 (class 2606 OID 16590)
-- Name: username_settings username_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.username_settings
    ADD CONSTRAINT username_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3511 (class 2606 OID 16413)
-- Name: dev_users dev_users_email_unique; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.dev_users
    ADD CONSTRAINT dev_users_email_unique UNIQUE (email);


--
-- TOC entry 3513 (class 2606 OID 16405)
-- Name: dev_users dev_users_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.dev_users
    ADD CONSTRAINT dev_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3519 (class 2606 OID 16513)
-- Name: end_user_settings end_user_settings_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_user_settings
    ADD CONSTRAINT end_user_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3517 (class 2606 OID 16431)
-- Name: end_users end_users_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_users
    ADD CONSTRAINT end_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3530 (class 2606 OID 16421)
-- Name: projects dev_user; Type: FK CONSTRAINT; Schema: projects; Owner: postgres
--

ALTER TABLE ONLY projects.projects
    ADD CONSTRAINT dev_user FOREIGN KEY (dev_user) REFERENCES users.dev_users(id) NOT VALID;


--
-- TOC entry 3534 (class 2606 OID 16710)
-- Name: refresh_token_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.refresh_token_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3533 (class 2606 OID 16715)
-- Name: session_token_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.session_token_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3536 (class 2606 OID 16720)
-- Name: username_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.username_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3535 (class 2606 OID 16725)
-- Name: password_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.password_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3537 (class 2606 OID 32798)
-- Name: security_questions_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.security_questions_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3532 (class 2606 OID 16522)
-- Name: end_user_settings end_user_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_user_settings
    ADD CONSTRAINT end_user_id FOREIGN KEY (end_user_id) REFERENCES users.end_users(id) ON DELETE CASCADE;


--
-- TOC entry 3538 (class 2606 OID 32803)
-- Name: security_questions end_user_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.security_questions
    ADD CONSTRAINT end_user_id FOREIGN KEY (end_user_id) REFERENCES users.end_users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3531 (class 2606 OID 16517)
-- Name: end_users project_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_users
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE;


-- Completed on 2023-05-14 20:24:37 BST

--
-- PostgreSQL database dump complete
--

