--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

-- Started on 2023-05-14 20:22:38 BST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3684 (class 0 OID 16416)
-- Dependencies: 219
-- Data for Name: projects; Type: TABLE DATA; Schema: projects; Owner: postgres
--

INSERT INTO projects.projects (id, name, dev_user) OVERRIDING SYSTEM VALUE VALUES (57, 'Project 1', 9);
INSERT INTO projects.projects (id, name, dev_user) OVERRIDING SYSTEM VALUE VALUES (60, 'Project 3', 9);


--
-- TOC entry 3694 (class 0 OID 16568)
-- Dependencies: 229
-- Data for Name: password_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

INSERT INTO settings.password_settings (id, project_id, regex, min_length, max_length, hash_length) OVERRIDING SYSTEM VALUE VALUES (0, 57, '.*', 2, 20, 16);
INSERT INTO settings.password_settings (id, project_id, regex, min_length, max_length, hash_length) OVERRIDING SYSTEM VALUE VALUES (3, 60, '.*', 5, 20, 16);


--
-- TOC entry 3692 (class 0 OID 16554)
-- Dependencies: 227
-- Data for Name: refresh_token_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

INSERT INTO settings.refresh_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) OVERRIDING SYSTEM VALUE VALUES (0, 57, true, 16, 14, 32, 7);
INSERT INTO settings.refresh_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) OVERRIDING SYSTEM VALUE VALUES (3, 60, false, 16, 14, 32, 7);


--
-- TOC entry 3698 (class 0 OID 24607)
-- Dependencies: 233
-- Data for Name: security_questions_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

INSERT INTO settings.security_questions_settings (id, n_questions, project_id, is_active, is_user_editable, min_n_questions, max_n_questions) OVERRIDING SYSTEM VALUE VALUES (0, 3, 57, true, true, 1, 3);
INSERT INTO settings.security_questions_settings (id, n_questions, project_id, is_active, is_user_editable, min_n_questions, max_n_questions) OVERRIDING SYSTEM VALUE VALUES (1, 1, 60, true, false, 1, 2);


--
-- TOC entry 3690 (class 0 OID 16538)
-- Dependencies: 225
-- Data for Name: session_token_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

INSERT INTO settings.session_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) OVERRIDING SYSTEM VALUE VALUES (0, 57, true, 20, 14, 28, 1);
INSERT INTO settings.session_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) OVERRIDING SYSTEM VALUE VALUES (3, 60, false, 100, 14, 28, 1);


--
-- TOC entry 3696 (class 0 OID 16583)
-- Dependencies: 231
-- Data for Name: username_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

INSERT INTO settings.username_settings (id, project_id, regex, min_length, max_length) OVERRIDING SYSTEM VALUE VALUES (0, 57, '.*', 2, 30);
INSERT INTO settings.username_settings (id, project_id, regex, min_length, max_length) OVERRIDING SYSTEM VALUE VALUES (3, 60, '.*', 5, 30);


--
-- TOC entry 3682 (class 0 OID 16401)
-- Dependencies: 217
-- Data for Name: dev_users; Type: TABLE DATA; Schema: users; Owner: postgres
--

INSERT INTO users.dev_users (id, email, token, hash) VALUES (0, 'afda', NULL, NULL);
INSERT INTO users.dev_users (id, email, token, hash) VALUES (2, 'afdf', NULL, NULL);
INSERT INTO users.dev_users (id, email, token, hash) VALUES (9, 'tginkel1@gmail.com', 'LMXJNdlfV4upNwp0FqEhgd/J67n2t3z4LwIzZgFZdYPVL9aLCnFVPgDd/k6d/Mk7V3ayFCNVB4g2VOWdyVyFZzhZfeyP+Doyt3ed9y11j4JWx/xEwCjFBoE5NHqTT+r+35+0EQ==', '$argon2id$v=19$m=65536,t=3,p=4$HZCBtu9YbZtsanL+I5RLlg$59AlrbvZ6Z3YETCA+Jw+heclu0umSTdYualh+HZR8Jk');
INSERT INTO users.dev_users (id, email, token, hash) VALUES (190, 'tginkel4@gmail.com', 'hlcS9tweAxasoVGqDt0yj672wMxUYzpSCFaplNfCLYayhSkVJdotZKbRhj4m4nwXEhgQxDja2DQ+138fgfiv/DXa9zkFleVxhqDKjzfNnyZKcnItfrHEtR7Mf67fMz9SIs1dsw==', '$argon2id$v=19$m=65536,t=3,p=4$t9miyrxfWwfG+gQgafrFDA$KJUg5jgYlgvpJBhmncEw4p1H3H770jUwIZMus3Z+dcQ');
INSERT INTO users.dev_users (id, email, token, hash) VALUES (191, '123@gmail.com', 'qCGuUAyMpDUNwNT9GE1B6xSvG0izLltRLx92FyeQrC9T2Kl+rS3N9aV75nPg/VSxaQm4rrsWA3Unfk+RmsqLgsiDfeF1PKd2LRZMtrmG/pfazE0ualF3MMUJavRfxIU5IPjG/Q==', '$argon2id$v=19$m=65536,t=3,p=4$MWWliYkkTGQy56Kt+ccxWw$gvV0L+0MtZdfY7xt9ZI54bjsx/D6Q/aFz+N7c0xkgYw');
INSERT INTO users.dev_users (id, email, token, hash) VALUES (193, '1234@gmail.com', 'tTXAK+qPZ3MTxqYfCQvj7MvOQpT/n5tFm0nfXcLQ54/fQC50AueYN4mZ7jYrdvT1cl9Ct2UQttU5OxXzIzbnvbpt9zZShk6b9+yfQm4RheQUBpY/zCoNXPaF0F5PbpDwTqQhdA==', '$argon2id$v=19$m=65536,t=3,p=4$viCSKaiD8GdB9tyQJU7Lpg$qbDDPTSgs4dv9nDGRi3OUUMmlXCP7ncq1eFFnhg7Ve8');


--
-- TOC entry 3687 (class 0 OID 16473)
-- Dependencies: 222
-- Data for Name: end_user_settings; Type: TABLE DATA; Schema: users; Owner: postgres
--

INSERT INTO users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) OVERRIDING SYSTEM VALUE VALUES (15, 32, 4, 16, true, 1);
INSERT INTO users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) OVERRIDING SYSTEM VALUE VALUES (17, 36, 4, 16, true, 1);
INSERT INTO users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) OVERRIDING SYSTEM VALUE VALUES (18, 37, 4, 16, true, 1);
INSERT INTO users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) OVERRIDING SYSTEM VALUE VALUES (31, 50, 4, 16, true, 1);
INSERT INTO users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) OVERRIDING SYSTEM VALUE VALUES (32, 51, 4, 16, true, 1);


--
-- TOC entry 3686 (class 0 OID 16427)
-- Dependencies: 221
-- Data for Name: end_users; Type: TABLE DATA; Schema: users; Owner: postgres
--

INSERT INTO users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) OVERRIDING SYSTEM VALUE VALUES (36, 60, 'toni321', '$argon2id$v=19$m=65536,t=3,p=4$I3AiVt2j+ltXIO2VUiGg9g$qfEHkwJDgr6W4D5NMFOU8Q', 'token', NULL, NULL, NULL);
INSERT INTO users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) OVERRIDING SYSTEM VALUE VALUES (37, 60, 'toni2', '$argon2id$v=19$m=65536,t=3,p=4$CQYUi/1V3sUyc1TcMbzWfg$waW4FYGZMNTzMkNx4MnXdw', 'token', NULL, NULL, NULL);
INSERT INTO users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) OVERRIDING SYSTEM VALUE VALUES (32, 57, 'toni123', '$argon2id$v=19$m=65536,t=3,p=4$bWQytV0T4nj3hdC2mJkA0A$NMTGHzejK0xc4VX2tvkfVA', 'a', '2023-05-18 16:58:02.882', NULL, '2023-05-18 16:58:02.882');
INSERT INTO users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) OVERRIDING SYSTEM VALUE VALUES (50, 60, 'test3', '$argon2id$v=19$m=65536,t=3,p=4$GRO36zieF+fbsX5kh6uTRQ$oVewb5B6LZv0UfKCcMVDFQ', 'qPcHbY7IGtuE4d2ciVGTWV946C7tOm86dX6dK9fJO5SPO+5PRtT9tCbUiq0jdG0QfwgQ8XMSHFcMJiSlJoz5XJcMMZspKwTp+BMVevsAwK0CPdwMFChYJUykq9/CHHtBDz1y9w==', '2023-02-14 17:35:22.19', NULL, NULL);
INSERT INTO users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) OVERRIDING SYSTEM VALUE VALUES (51, 60, 'test3', '$argon2id$v=19$m=65536,t=3,p=4$h9c+vgYYUzlCgqdnq7RKfg$kAWoArkGUKhsvbVLLt2v2g', 'G103Aw8FqilZeJkF+mNoea1uJhrhLYX1x/LgU9vvZXQ0I9b1T87VVP58XuZOegNutJSE+/nCN2yugahM8u00UN8a/1NjrnXWy2xhm+c/ZTME/u73nVU+U2ExlEuwwE1HVeMXdw==', '2023-03-14 18:42:05.14', 'fvs6UCgtmWX21KlXlv6D0g==', '2023-03-16 18:42:05.14');


--
-- TOC entry 3700 (class 0 OID 24622)
-- Dependencies: 235
-- Data for Name: security_questions; Type: TABLE DATA; Schema: users; Owner: postgres
--

INSERT INTO users.security_questions (id, question, answer, end_user_id) OVERRIDING SYSTEM VALUE VALUES (44, 'Hello', 'Bye', 36);
INSERT INTO users.security_questions (id, question, answer, end_user_id) OVERRIDING SYSTEM VALUE VALUES (49, 'Yes', 'No', 37);
INSERT INTO users.security_questions (id, question, answer, end_user_id) OVERRIDING SYSTEM VALUE VALUES (61, 'a', 'aa', 32);
INSERT INTO users.security_questions (id, question, answer, end_user_id) OVERRIDING SYSTEM VALUE VALUES (62, 'b', 'bb', 32);


--
-- TOC entry 3706 (class 0 OID 0)
-- Dependencies: 218
-- Name: Projects_id_seq; Type: SEQUENCE SET; Schema: projects; Owner: postgres
--

SELECT pg_catalog.setval('projects."Projects_id_seq"', 178, true);


--
-- TOC entry 3707 (class 0 OID 0)
-- Dependencies: 228
-- Name: password_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.password_settings_id_seq', 120, true);


--
-- TOC entry 3708 (class 0 OID 0)
-- Dependencies: 226
-- Name: refresh_token_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.refresh_token_settings_id_seq', 120, true);


--
-- TOC entry 3709 (class 0 OID 0)
-- Dependencies: 232
-- Name: security_questions_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.security_questions_id_seq', 118, true);


--
-- TOC entry 3710 (class 0 OID 0)
-- Dependencies: 224
-- Name: session_token_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.session_token_settings_id_seq', 120, true);


--
-- TOC entry 3711 (class 0 OID 0)
-- Dependencies: 230
-- Name: username_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.username_settings_id_seq', 120, true);


--
-- TOC entry 3712 (class 0 OID 0)
-- Dependencies: 216
-- Name: dev_users_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.dev_users_id_seq', 193, true);


--
-- TOC entry 3713 (class 0 OID 0)
-- Dependencies: 223
-- Name: end_user_settings_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.end_user_settings_id_seq', 80, true);


--
-- TOC entry 3714 (class 0 OID 0)
-- Dependencies: 220
-- Name: end_users_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.end_users_id_seq', 99, true);


--
-- TOC entry 3715 (class 0 OID 0)
-- Dependencies: 234
-- Name: security_questions_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.security_questions_id_seq', 62, true);


-- Completed on 2023-05-14 20:22:38 BST

--
-- PostgreSQL database dump complete
--

