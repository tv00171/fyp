// Composables
import {createRouter, createWebHistory} from 'vue-router'
import {useSidebarStore} from "@/store/sidebarStore";

const routes = [
  {
    path: '/login/',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/sign-up/',
    component: () => import('@/views/Register.vue'),
  },
  {
    path: '/',
    component: () => import('@/views/layouts/Sidebar.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/Dashboard.vue')
      },
      {
        path: 'profile',
        name: 'Profile',
        component: () => import('@/views/dashboard/Dashboard.vue')
      },
      {
        path: 'project/:projectId',
        name: 'Dashboard Project',
        component: () => import('@/views/dashboard/Project.vue')
      },
      {
        path: '/project/:projectId/settings',
        name: 'Dashboard Project Settings',
        component: () => import('@/views/dashboard/ProjectSettings.vue')
      },
      {
        path: '/endUsers/:endUserId/settings',
        name: 'Dashboard EndUser Settings',
        component: () => import('@/views/dashboard/EndUserSettings.vue')
      },
    ]
  },
  {
    path: '/project/login',
    name: 'Project Auth Login',
    component: () => import('@/views/projectAuth/projectLogin.vue')
  },
  {
    path: '/project/register',
    name: 'Project Auth Register',
    component: () => import('@/views/projectAuth/projectRegister.vue')
  }
]


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  // @ts-ignore
  routes,
})

router.beforeEach(async (to) => {
  const sidebarStore = useSidebarStore();
  sidebarStore.page = to.name;
})
export default router
