import {defineStore} from 'pinia'
import router from '@/router/index.js'
// import i18n from "@/i18n";

function updateUser(user: any) {
  localStorage.setItem('user', JSON.stringify(user))
}

/**
 * State management of the user. This will be global and the state will not be reset unless the page is reset.
 */
export const useUserStore = defineStore('userStore', {
  state: () => ({
    login_error: null,
    user: null,
    fetch_error: null
  }),
  getters: {
    getUser: state => {
      state.user
    }
  },
  actions: {
     setUser(user:any){
      localStorage.setItem('user', JSON.stringify(user));
      this.user = user;
    },
    async logout(){
       localStorage.clear();
       this.user = null;
    },
    isAuthenticated(){
       if(this.user == null){
         return false;
       }
       else{
         return true;
       }
    }
  }
})
