// Utilities
import {defineStore} from 'pinia'
import axios from 'axios'


export const useProjectViewStore = defineStore('projectViewStore', {
  state: () => ({
    fetch_error: null,
    isLoading: false,
    projectId: null,
    endUsers: null,
    create_modal: false,
    newUserName: null,
    newUserPass: null
  }),
  actions: {
    async getEndUsers() {
      try {
        this.isLoading = true;
        // Make the api call to get end users
        this.endUsers = await axios.get(`/endUsers/inProject/${this.projectId}`);
      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isLoading = false
      }
    },
    async createEndUser() {
      try {
        this.isLoading = true;
        await axios.post('/endUsers/create', {
          projectId: this.projectId,
          username: this.newUserName,
          password: this.newUserPass
        })
      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isLoading = false;
        this.create_modal = false;
        this.newUserName = null;
        this.newUserPass = null;
        await this.getEndUsers()
      }
    }
  }
})
