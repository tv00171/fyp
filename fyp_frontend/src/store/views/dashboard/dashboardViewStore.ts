// Utilities
import {defineStore} from 'pinia'
import axios from 'axios'
import {useUserStore} from "@/store/useUser";


export const useDashboardViewStore = defineStore('dashboardViewStore', {
  state: () => ({
    fetch_error: null,
    isLoading: false,
    projects: null,
    newProjectName: null,
  }),
  actions: {
    async getProjects() {
      const userStore = useUserStore();
      try {
        // Set the loading state of the page to truew
        this.isLoading = true;
        this.projects = await axios.get('/projects/', {headers: {token: userStore.user.token}},);
      } catch (e: any) {
        this.fetch_error = e;
      } finally {
        this.isLoading = false
      }

    },
    async createProject() {
      try {
        this.isLoading = true
        await axios.post('/projects/createProject', {
          name: this.newProjectName
        },);
      } catch (e) {
        this.fetch_error = e;
      } finally {
        this.isLoading = false
        await this.getProjects();
      }
    }
  }
})
