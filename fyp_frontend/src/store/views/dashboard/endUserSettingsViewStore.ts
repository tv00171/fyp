// Utilities
import {defineStore} from 'pinia'
import axios from 'axios'


export const useEndUserSettingsViewStore = defineStore('endUserSettingsViewStore', {
  state: () => ({
    fetch_error: null,
    isLoading: false,
    endUserId: null,
    settings: null,
    isSettingsLoading: false,
    isSaveSuccess: false,
    newSecurityQuestions: null,
    question: null,
    answer: null
  }),
  actions: {
    async getSettings() {
      try {
        this.isLoading = true;
        const response = await axios.get(`/endUsers/${this.endUserId}/settings`);
        this.settings = response
        // Check if the user
        if (this.settings.projectSettings.authentication_methods.security_questions.is_user_editable == false) {
          if (this.settings.securityQuestions.length < this.settings.projectSettings.authentication_methods.security_questions.n_questions) {
            for (let i = 0; i <= this.settings.projectSettings.authentication_methods.security_questions.n_questions - this.settings.securityQuestions.length; i++) {
              this.settings.securityQuestions.push({question: null, answer: null})
            }
          } else if (this.settings.securityQuestions.length > this.settings.projectSettings.authentication_methods.security_questions.n_questions) {
            for (let i = 0; i <= this.settings.securityQuestions.length - this.settings.projectSettings.authentication_methods.security_questions.n_questions; i++) {
              this.settings.securityQuestions.pop();
            }
          }


        }

      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isLoading = false
      }
    },

    async changeSettings() {
      try {
        this.fetch_error = null;
        this.isSettingsLoading = true;
        if (this.settings.securityQuestions.length > this.settings.projectSettings.authentication_methods.security_questions.max_n_questions) {
          throw `You have too many security questions, maximum amount allowed is ${this.settings.projectSettings.authentication_methods.security_questions.max_n_questions}`
        }
        await axios.post(`/endUsers/${this.endUserId}/changeSettings`, {
          settings: {
            ...this.settings.endUserSettings,
            security_questions: this.settings.securityQuestions
          },
        });
      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isSettingsLoading = false
        this.isSaveSuccess = true;
      }
    }
  }
})
