// Utilities
import {defineStore} from 'pinia'
import axios from 'axios'


export const useProjectSettingsViewStore = defineStore('projectSettingsViewStore', {
  state: () => ({
    fetch_error: null,
    isLoading: false,
    projectId: null,
    settings: null,
    isSettingsLoading: false,
    isSaveSuccess: false
  }),
  actions: {
    async getProjectSettings() {
      try {
        this.isLoading = true;
        const response = await axios.get(`/projects/projectSettings/${this.projectId}`);
        // @ts-ignores
        this.settings = response.settings
      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isLoading = false
      }
    },

    async changeSettings() {
      try {
        this.isSettingsLoading = true;
        await axios.post(`/projects/changeSettings/${this.projectId}`, {
          settings: this.settings
        });
      } catch (e) {
        this.fetch_error = e
      } finally {
        this.isSettingsLoading = false
        this.isSaveSuccess = true;
      }
    }
  }
})
