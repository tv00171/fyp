import {defineStore} from 'pinia'
import axios from 'axios'
import router from "@/router";


export const useProjectLoginViewStore = defineStore('projectLoginViewStore', {
  state: () => ({
    login_error: null,
    fetch_error: null,
    isLoading: false,
    allProjects: null,
    currentProject: null,
    projectSettings: null,
    username: null,
    authentication_method: null,
    security_questions: null,
    isPageLoading: false,
    password: null
  }),
  actions: {
    async getProjectSettings() {
      this.login_error = null
      try {
        this.isPageLoading = true;
        this.password = null;
        this.username = null;
        this.authentication_method = null
        this.projectSettings = await axios.get(`/projects/projectSettings/${this.currentProject}`);
      } catch (e) {
        this.login_error = e;
      } finally {
        this.isPageLoading = false;
      }
    },
    async getAllProjects() {
      this.login_error = null
      try {
        // Set the loading state of the page to true
        this.isPageLoading = true;
        // Make the api call to log in
        this.allProjects = await axios.get(`/projects/`);
      } catch (e: any) {
        this.fetch_error = e;
      } finally {
        this.isPageLoading = false
      }
    },
    async login(password: string) {
      this.login_error = null
      try {
        // Set the loading state of the page to true
        this.isLoading = true;
        if (this.authentication_method == 'password') {
          // Make the api call to log in
          await axios.post(`/projects/${this.currentProject}/login/`, {
            username: this.username,
            password: this.password,
            authentication_method: "password"
          });

          // Push the application to the home page
          await router.push('/dashboard/');
        } else if (this.authentication_method == 'security_questions') {
          await axios.post(`/projects/${this.currentProject}/login/`, {
            username: this.username,
            security_questions: this.security_questions,
            authentication_method: "security_questions"
          });

          // Push the application to the home page
          await router.push('/dashboard/');
        }
      } catch (e: any) {
        this.login_error = e;
      } finally {
        this.isLoading = false
      }

    },
    async getSecurityQuestions() {
      this.login_error = null
      try {
        this.isPageLoading = true;
        this.security_questions = await axios.get(`endUsers/${this.currentProject}/${this.username}/securityQuestions/`);
        // this.isPageLoading = false;
        this.authentication_method = 'security_questions'
      } catch (e) {
        this.login_error = e
      } finally {
        this.isPageLoading = false;
      }
    }
  }
})
