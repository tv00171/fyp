import {defineStore} from 'pinia'
import axios from 'axios'


export const useProjectRegisterViewStore = defineStore('projectRegisterViewStore', {
  state: () => ({
    register_error: null,
    fetch_error: null,
    isLoading: false,
    allProjects: null,
    currentProject: null,
    projectSettings: null,
    username: null,
    authentication_method: null,
    security_questions: null,
    isPageLoading: false,
    password: null
  }),
  actions: {
    async getProjectSettings() {
      this.register_error = null
      try {
        this.isPageLoading = true;
        this.password = null;
        this.username = null;
        this.authentication_method = null
        this.projectSettings = await axios.get(`/projects/projectSettings/${this.currentProject}`);
        console.log(this.projectSettings.settings.authentication_methods.security_questions.n_questions)
        this.security_questions = [];
        if (this.projectSettings.settings.authentication_methods.security_questions.is_active == true && this.projectSettings.settings.authentication_methods.security_questions.is_user_editable == false) {
          for (let i = 0; i < this.projectSettings.settings.authentication_methods.security_questions.n_questions; i++) {
            this.security_questions.push({question: null, answer: null});
          }
        }
      } catch (e) {
        this.register_error = e;
      } finally {
        this.isPageLoading = false;
      }
    },
    async getAllProjects() {
      this.register_error = null
      try {
        // Set the loading state of the page to true
        this.isPageLoading = true;
        // Make the api call to log in
        this.allProjects = await axios.get(`/projects/`);
      } catch (e: any) {
        this.fetch_error = e;
      } finally {
        this.isPageLoading = false
      }
    },
    async register() {
      this.register_error = null;
      try {
        this.isPageLoading = true;
        await axios.post(`/endUsers/create`, {
          username: this.username,
          password: this.password,
          projectId: this.currentProject,
          security_questions: this.security_questions
        });
      } catch (e) {
        this.register_error = e;
      } finally {
        this.isPageLoading = false;
      }
    },
    async getSecurityQuestions() {
      this.register_error = null
      try {
        this.isPageLoading = true;
        this.security_questions = await axios.get(`endUsers/${this.currentProject}/${this.username}/securityQuestions/`);
        // this.isPageLoading = false;
        this.authentication_method = 'security_questions'
      } catch (e) {
        this.register_error = e
      } finally {
        this.isPageLoading = false;
      }
    }
  }
})
