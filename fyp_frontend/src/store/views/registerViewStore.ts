// Utilities
import {defineStore} from 'pinia'
import axios from 'axios'
import router from "@/router";
import {useUserStore} from "@/store/useUser";


export const useRegisterViewStore = defineStore('registerViewStore', {
  state: () => ({
    login_error: null,
    isLoading: false,
  }),
  actions: {
    async register(email: string, password: string) {
      const userStore = useUserStore();
      try {
        // Set the loading state of the page to true
        this.isLoading = true;

        // Make the api call to register a user
        const loginResponse = await axios.post('/auth/register/', {
          email: email,
          password: password
        });

        // If it got here it means the call was a success, so set the local storage to the logged in user
        userStore.setUser(loginResponse);
        // Push the application to the home page
        await router.push('/dashboard/');
      } catch (e: any) {
        this.login_error = e;
      } finally {
        this.isLoading = false
      }

    }
  }
})
