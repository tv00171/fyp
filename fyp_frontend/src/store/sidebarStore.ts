// Utilities
import { defineStore } from 'pinia'
import router from "@/router";
import {useUserStore} from "@/store/useUser";

export const useSidebarStore = defineStore('sidebarStore', {
  state: () => ({
    error: null,
    isLoading: false,
    page: null
  }),
  actions:{
    async logout (){
      const userStore = useUserStore();
      try{
        // Set the loading state of the page to true
        this.isLoading = true;
        await userStore.logout();
        // Push the application to the home page
        await router.push('/login/');
      } catch (e:any) {
        this.error = e;
      }
      finally {
        this.isLoading = false
      }
      }
    }
})
