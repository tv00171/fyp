
// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import {createVuetify, ThemeDefinition} from 'vuetify'


const light: ThemeDefinition ={
  dark: false,
  colors:{
    primary: '#21c018',
    secondary:'#ffffff',
    background: '#e1e1e1',
  }
};
const dark: ThemeDefinition ={
  dark: true,
  colors:{
    primary: '#21c018',
    secondary:'#212121',
    error: '#ef0404'
  }
};

export default createVuetify({
  theme: {
    defaultTheme: 'light',
    themes: {
      light,
      dark
    },
  },
})
