import en from "./en.json";
import {createI18n} from "vue-i18n";
export const messages = {
  "en": en,
};

// Create localisation settings
export default createI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages, // set locale messages
})
