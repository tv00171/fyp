import { Router } from 'express';
import AuthService from "../services/authService";
import BaseError from "../models/BaseError";

const routes = Router();

    routes.get('/', (req, res) => {
        return res.json({ message: 'Hello World this is the test' });
    });

    routes.post('/login', async (req, res) => {
        try{
            const {email, password} = req.body;

            // Check the email and password are provided
            if(email == null  || password == null){throw new BaseError({error: 'Missing email or password', errno: 5, status: 400})}

           // Login the user
            const result = await AuthService.login(email, password);

            // Return the user information
            return res.json({success: true, payload:result});
        }
        catch (e) {
            if(e instanceof BaseError){
                return res.status(e.status).json(e.toJSON())
            }
            return res.status(500).json({success: false, error: e.toString(), errno: 6})
        }
    });
    routes.post('/register', async (req, res) => {
        try{
            const {email, password} = req.body;
            const result = await AuthService.register(email, password);
            return res.json({success: true, payload:result});
        }
        catch (e) {
            if(e instanceof BaseError){
                return res.status(e.status).json(e.toJSON())
            }
            return res.status(500).json({success: false, error: e, errno: 6})
        }
});
export default routes;
