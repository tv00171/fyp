import {Router} from 'express';
import EndUserService from "../services/endUserService";
import BaseError from "../models/BaseError";
import ProjectService from "../services/projectService";

const routes = Router();

/**
 * Get all the end-users in the DB
 */
routes.get('/', async (req, res) => {
    try {
        const endUsers = await EndUserService.getAllEndUsers();
        return res.json({success: true, payload: endUsers});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
/**
 * Get all the end-users of a project
 */
routes.get('/inProject/:projectId', async (req, res) => {
    try {
        const projectId = req.params.projectId
        if (projectId == null) {
            throw new BaseError({error: "Missing project ID", errno: 5, status: 400})
        }
        const endUsers = await EndUserService.getProjectEndUsers(+projectId);
        return res.json({success: true, payload: endUsers});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

/**
 * Get the settings of the endUser and the configuration options. Also get the project Settings to tell the user the
 * scope of the changes he is allowed to make.
 */
routes.get('/:endUserId/settings', async (req, res) => {
    try {
        const endUserId = req.params.endUserId;
        if (endUserId == null) {
            throw new BaseError({error: "Missing endUser ID", errno: 5, status: 400})
        }
        const endUserSettings = await EndUserService.getEndUserSettings(+endUserId);
        // Since we don't have the projectId we need to get it from the endUser.
        const projectId = await EndUserService.getProjectIdFromEndUserId(+endUserId);
        const projectSettings = await ProjectService.getProjectSettings(projectId);
        const endUserSecurityQuestions = await EndUserService.getEndUserSecurityQuestions(+endUserId)

        return res.json({
            success: true,
            payload: {
                projectSettings: projectSettings.settings,
                endUserSettings: endUserSettings.settings,
                securityQuestions: endUserSecurityQuestions
            }
        });
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});


/**
 * Change the current settings of the endUser. The settings will have to be within the bounds of the projectSettings
 */
routes.post('/:endUserId/changeSettings', async (req, res) => {
    try {
        const endUserId = req.params.endUserId;
        const {settings} = req.body
        if (endUserId == null) {
            throw new BaseError({error: "Missing endUser ID", errno: 5, status: 400})
        }
        if (settings == null) {
            throw new BaseError({error: "Missing settings", errno: 5, status: 400})
        } else if (settings.session_token_duration == null) {
            throw new BaseError({error: "Missing session token", errno: 5, status: 400})
        } else if (settings.refresh_token_duration == null) {
            throw new BaseError({error: "Missing refresh token", errno: 5, status: 400})
        }

        await EndUserService.changeSettings(+endUserId, settings);

        return res.json({
            success: true
        });
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

/**
 * Create an end user and its own custom settings
 */
routes.post('/create', async (req, res) => {
    try {
        const {username, password, projectId, security_questions} = req.body;
        if (username == null || password == null || projectId == null) {
            throw new BaseError({errno: 5, error: "Missing parameters", status: 400})
        }
        const endUsers = await EndUserService.createEndUser(username, password, projectId, {security_questions});
        return res.json({success: true, payload: endUsers});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

/**
 * Authenticate the validity of a token, and if it is valid then return the user
 */
routes.post('/validateToken', async (req, res) => {
    try {
        const {token, projectId} = req.body;
        if (token == null || projectId == null) {
            throw new BaseError({errno: 5, error: "Missing parameters", status: 400})
        }
        const endUser = await EndUserService.authenticateToken(projectId, token)
        return res.json({success: true, payload: endUser.toJson()});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
/**
 * Use the refresh token to generate new tokens that the user can use without having to login.
 * This will generate a new session token and a new refresh token
 */
routes.post('/refreshTokens', async (req, res) => {
    try {
        const {refresh_token, projectId} = req.body;
        if (refresh_token == null || projectId == null) {
            throw new BaseError({errno: 5, error: "Missing parameters", status: 400})
        }
        const newTokens = await EndUserService.refreshToken(refresh_token, +projectId)
        return res.json({success: true, payload: newTokens});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
/**
 * Get the security questions of an endUser for the purposes of authentication. This will only return the questions and
 * not the answers.
 */
routes.get('/:projectId/:username/securityQuestions', async (req, res) => {
    try {
        const endUserUsername = req.params.username
        const projectId = req.params.projectId
        if (endUserUsername == null) {
            throw new BaseError({errno: 5, error: "Missing parameters", status: 400})
        }
        const endUserId = await EndUserService.getEndUserIdFromUsername(endUserUsername, +projectId)
        const endUserSecurityQuestions = await EndUserService.getEndUserSecurityQuestions(endUserId);

        const userSettings = await EndUserService.getEndUserSettings(endUserId);
        if (userSettings.settings.use_security_questions == false) {
            throw new BaseError({errno: 22, error: "Security questions not enabled for this user", status: 401})
        }
        // Only send down the questions and not the answers
        let questions = []
        for (let question of endUserSecurityQuestions) {
            questions.push({id: question.id, question: question.question})
        }

        return res.json({success: true, payload: questions});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

export default routes;
