import {Router} from 'express';

const routes = Router();

routes.get('/', (req, res) => {
    return res.json({message: 'Hello World this is the test'});
});

export default routes;
