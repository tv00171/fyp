import {Router} from 'express';
import ProjectService from "../services/projectService";
import BaseError from "../models/BaseError";
import EndUserService from "../services/endUserService";

const routes = Router();

/**
 * Get all the projects in the db
 */
routes.get('/', async (req, res) => {
    try {
        console.log(res.locals.user.id)
        const projects = await ProjectService.getAllProjects(res.locals.user.id);
        return res.json({payload: projects});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

/**
 * Get the settings of the project
 */
routes.get('/projectSettings/:projectId', async (req, res) => {
    try {
        const projectId = req.params.projectId;

        if (projectId === undefined) {
            throw new BaseError({error: "Missing project id", errno: 5, status: 400})
        }
        const projectSettings = await ProjectService.getProjectSettings(+projectId);
        const project = await ProjectService.getProject(+projectId);
        return res.json({payload: {...projectSettings, name: project.name}});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
/**
 * Change the settings of an already existing project
 */
routes.post('/changeSettings/:projectId', async (req, res) => {
    try {
        const projectId = req.params.projectId;
        const {settings} = req.body;
        if (projectId == null) {
            throw new BaseError({error: "Missing project id", errno: 5, status: 400})
        }
        if (settings == null) {
            throw new BaseError({error: "Missing settings parameter", errno: 5, status: 400})
        }
        const projectSettings = await ProjectService.changeProjectSettings(+projectId, settings);
        return res.json({payload: projectSettings});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
/**
 * Create a project in the db and the corresponding configuration
 */
routes.post('/createProject', async (req, res) => {
    try {
        const {name, settings} = req.body
        if (name == null) {
            throw new BaseError({error: "Missing name parameter", errno: 5, status: 400})
        }
        const projects = await ProjectService.createProject(name, res.locals.user.id, settings);
        return res.json({success: true, payload: projects});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});

/**
 * Project Login. Login in an endUser into a project using their custom settings. The settings used will be the user settings as
 * if the project settings have been updated it might not be possible for the endUser to login.
 */
routes.post('/:projectId/login', async (req, res) => {
    try {
        const projectId = req.params.projectId;
        const {authentication_method} = req.body;
        if (authentication_method == null) {
            throw new BaseError({error: "Missing authentication method parameter", errno: 5, status: 400})
        }
        if (projectId == null) {
            throw new BaseError({error: "Missing projectId parameter", errno: 5, status: 400})
        }

        let user;
        if (authentication_method == 'password') {
            // Login using password
            const {username, password} = req.body;
            if (username == null || password == null) {
                throw new BaseError({error: "Missing username or password parameter", errno: 5, status: 400})
            }
            user = await ProjectService.loginProjectUsingPassword(username, password, +projectId)
        } else if (authentication_method == 'security_questions') {
            // Login using the security questions
            const {security_questions, username} = req.body;
            if (security_questions == null || username == null) {
                throw new BaseError({error: "Missing security questions or username", errno: 5, status: 400})
            }
            user = await ProjectService.loginProjectUsingSecurityQuestions(username, security_questions, +projectId)

        } else {
            throw new BaseError({errno: 5, error: 'Authentication method not know', status: 400})
        }

        // Restart the token duration countdown.
        await EndUserService.restartTokensTimers(user.id);
        return res.json({success: true, payload: user});
    } catch (e) {
        if (e instanceof BaseError) {
            return res.status(e.status).json(e.toJSON())
        }
        return res.status(500).json({success: false, error: e.toString(), errno: 6})
    }
});
export default routes;
