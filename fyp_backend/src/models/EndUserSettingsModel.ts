import BaseError from "./BaseError";
import {ResultRow} from "ts-postgres";

interface ISettings {
    session_token_duration: number,
    refresh_token_duration: number,
    use_security_questions: boolean,
    n_security_questions: number
}

export default class EndUserSettingsModel {
    /**
     * The id that identifies the user in the DB
     */
    id: number;
    /**
     * The project the settings belongs to
     */
    endUserId: number;
    /**
     * The settings of the project
     */
    settings: ISettings;

    constructor(fields: { id: number, endUserId: number, settings: ISettings }) {
        this.id = fields.id;
        this.endUserId = fields.endUserId;
        this.settings = fields.settings;
    }

    static fromDB(row: ResultRow) {
        try {
            const newObject = new EndUserSettingsModel({
                id: +row.get("id").toString(),
                endUserId: +row.get("end_user_id").toString(),
                settings: {
                    session_token_duration: +row.get("session_token_duration").toString(),
                    refresh_token_duration: +row.get("refresh_token_duration").toString(),
                    use_security_questions: row.get("use_security_questions").toString().toLowerCase() == 'true',
                    n_security_questions: +row.get("n_security_questions").toString(),
                }
            })
            return newObject

        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

}
