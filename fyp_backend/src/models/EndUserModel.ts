import BaseError from "./BaseError";
import {ResultRow} from "ts-postgres";

export default class EndUserModel {
    /**
     * The id that identifies the user in the DB
     */
    id: number;
    /**
     * The Id of the project the end-user belongs to
     */
    projectId: number;
    /**
     * The username of the end user
     */
    username: string;
    /**
     * The hashed password of the endUser
     */
    password: string;
    /**
     * The token of the endUser
     */
    token: string;
    /**
     * The expiration date of the endUser Token
     */
    token_expires?: string;
    refresh_token?: string;
    refresh_token_expires?: string;


    constructor(fields: { id: number, projectId: number, username: string, token: string, token_expires?: string, refresh_token?: string, password: string, refresh_token_expires?: string }) {
        this.projectId = fields.projectId;
        this.id = fields.id;
        this.username = fields.username;
        this.password = fields.password;
        this.token_expires = fields.token_expires
        this.token = fields.token;
        this.refresh_token = fields.refresh_token;
        this.refresh_token_expires = fields.refresh_token_expires;
    }

    static fromJSON(json: any) {
        try {
            return new EndUserModel({
                id: json.id,
                projectId: json.projectId,
                username: json.username,
                token: json.token,
                password: json.password
            });
        } catch (e) {
            throw new BaseError({error: e, errno: 5});
        }
    }

    /**
     * Parse endUser from the DB to the class
     * @param row
     */
    static fromDB(row: ResultRow) {
        try {
            return new EndUserModel({
                id: +row.get("id").toString(),
                projectId: +row.get("project_id").toString(),
                username: row.get("username").toString(),
                token: row.get("token").toString(),
                token_expires: row.get("token_expires") != null ? row.get("token_expires").toString() : null,
                password: row.get("password").toString(),
                refresh_token: row.get("refresh_token") != null ? row.get("refresh_token").toString() : null,
                refresh_token_expires: row.get("refresh_token_expires") != null ? row.get("refresh_token_expires").toString() : null,
            });
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 5});
        }
    }

    toJson() {
        return {
            id: this.id,
            username: this.username,
            projectId: this.projectId,
            session: {
                session_token: this.token,
                refresh_token: this.refresh_token,
            }
        }
    }

}
