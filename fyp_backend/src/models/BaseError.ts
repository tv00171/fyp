/**
 * The different possible error codes for examfy errors and their meaning.
 * The first number will indicate the type of error:
 *
 * Error response:
 *
 * {
 *
 *     success: false,
 *
 *     errno: xxx,
 *
 *     error: This is the error message
 *
 * }
 */

class BaseError {
    // The error outputed by the computer
    error: string;
    // The error code using the Examfy Error codes
    errno: number;
    // A human-friendly error message that describe the error in one sentence
    message: string;
    status: number = 500;
    private _error_codes: any = {
        1: 'Can\'t connect to the db',
        2: 'Does not have the correct role for this operation',
        3: 'Is not logged in / Invalid token',
        4: 'Token has expired',
        5: 'Request does not have all the necessary parameters',
        6: 'There was an unexpected error',
        7: 'Wrong credentials',
        8: 'Invalid db query',
        9: 'There was an error hashing the password',
        10: 'Password does not match the required criteria',
        11: 'Username does not match all the necessary criteria',
        12: 'User does not have access to this resources',
        13: 'The requested resource could not be found',
        14: 'There are no students',
        15: 'Document contains invalid format',
        16: 'No data to be exported',
        17: 'Error parsing the model from the DB',
        18: 'End User settings are outside project bounds',
        19: 'End User settings are not editable',
        20: 'Wrong security questions asnwers',
        21: 'Session Token has expired',
        22: 'Authentication Method not available',
        other: 'Unknown errno'
    }

    constructor(fields: { errno: number, error: string, status?: number }) {
        this.errno = fields.errno;
        this.error = fields.error;
        if (fields.status != undefined) {
            this.status = fields.status;
        }
        this.message = this._error_codes[fields.errno];
        if (this.message == undefined) {
            this.message = this._error_codes.other
        }
    }

    toJSON() {
        return {success: false, error: this.error, errno: this.errno, message: this.message}
    }

}

export = BaseError
