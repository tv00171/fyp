import BaseError from "./BaseError";
import {ResultRow} from "ts-postgres";

interface ISettings {
    username?: {
        regex: string,
        max_length: number,
        min_length: number,
    },
    password?: {
        regex: string,
        min_length: number,
        max_length: number,
        hash_length: number,
    },
    session_token?: {
        default_duration: number,
        min_duration: number,
        max_duration: number,
        length: number,
        is_user_editable: boolean
    },
    refresh_token?: {
        length: number,
        default_duration: number,
        min_duration: number,
        max_duration: number
        is_user_editable: boolean
    },
    authentication_methods?: {
        security_questions?: {
            n_questions: number,
            is_active: boolean,
            is_user_editable: boolean
            min_n_questions: number,
            max_n_questions: number,
        }
    }
}

export default class ProjectSettingsModel {
    /**
     * The id that identifies the user in the DB
     */
    id?: number;
    /**
     * The project the settings belongs to
     */
    projectId: number;
    /**
     * The settings of the project
     */
    settings: ISettings;

    constructor(fields: { id?: number, projectId: number, settings?: ISettings }) {
        this.id = fields.id;
        this.projectId = fields.projectId;
        this.settings = fields.settings;
        if (this.settings == null) {
            this.settings = {authentication_methods: {}};
        }
    }

    static fromDB(row: ResultRow) {
        try {
            const newObject = new ProjectSettingsModel({
                id: +row.get("id").toString(),
                projectId: +row.get("project_id").toString(),
                settings: {
                    username: {
                        regex: row.get("username_regex").toString(),
                        max_length: +row.get("username_max_length").toString(),
                        min_length: +row.get("username_min_length").toString(),
                    },
                    refresh_token: {
                        length: +row.get("refresh_token_length").toString(),
                        max_duration: +row.get("refresh_token_max_duration").toString(),
                        min_duration: +row.get("refresh_token_min_duration").toString(),
                        default_duration: +row.get("refresh_token_default_duration").toString(),
                        is_user_editable: row.get("refresh_token_is_user_editable").toString().toLowerCase() == 'true'
                    },
                    session_token: {
                        default_duration: +row.get("token_default_duration").toString(),
                        max_duration: +row.get("token_max_duration").toString(),
                        min_duration: +row.get("token_min_duration").toString(),
                        length: +row.get("token_length").toString(),
                        is_user_editable: row.get("token_is_user_editable").toString().toLowerCase() == 'true',
                    },
                    password: {
                        regex: row.get("password_regex").toString(),
                        hash_length: +row.get("default_hash_length").toString(),
                        min_length: +row.get("password_min_length").toString(),
                        max_length: +row.get("password_max_length").toString(),
                    },
                }
            })
            return newObject

        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

    /**
     * Parse the username DB values to a JSON and add them to the projectSettings object.
     * Only does username data
     * @param row
     */
    static usernameSettingsFromDB(row: ResultRow) {
        try {
            return {
                regex: row.get("regex").toString(),
                max_length: +row.get("max_length").toString(),
                min_length: +row.get("min_length").toString(),
            }
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

    /**
     * Parse the username DB values to a JSON and add them to the projectSettings object.
     * Only does password data
     * @param row
     */
    static passwordSettingsFromDB(row: ResultRow) {
        try {
            return {
                regex: row.get("regex").toString(),
                hash_length: +row.get("hash_length").toString(),
                min_length: +row.get("min_length").toString(),
                max_length: +row.get("max_length").toString(),
            }
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

    /**
     * Parse the username DB values to a JSON and add them to the projectSettings object.
     * Only does refresh token data
     * @param row
     */
    static refreshTokenSettingsFromDB(row: ResultRow) {
        try {
            return {
                length: +row.get("length").toString(),
                max_duration: +row.get("max_duration").toString(),
                min_duration: +row.get("min_duration").toString(),
                default_duration: +row.get("default_duration").toString(),
                is_user_editable: row.get("is_user_editable").toString().toLowerCase() == 'true'
            }
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

    /**
     * Parse the username DB values to a JSON and add them to the projectSettings object.
     * Only does session token data
     * @param row
     */
    static sessionTokenSettingsFromDB(row: ResultRow) {
        try {
            return {
                default_duration: +row.get("default_duration").toString(),
                max_duration: +row.get("max_duration").toString(),
                min_duration: +row.get("min_duration").toString(),
                length: +row.get("length").toString(),
                is_user_editable: row.get("is_user_editable").toString().toLowerCase() == 'true',
            }
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }

    static securityQuestionsFromDB(row: ResultRow) {
        try {
            return {
                max_n_questions: +row.get("max_n_questions").toString(),
                min_n_questions: +row.get("min_n_questions").toString(),
                n_questions: +row.get("n_questions").toString(),
                is_active: row.get("is_active").toString().toLowerCase() == 'true',
                is_user_editable: row.get("is_user_editable").toString().toLowerCase() == 'true',
            }
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 17});
        }
    }


    getSettings() {
        const names = 'username_regex,' + 'password_regex' +
            'username_min_length,' +
            'username_max_length,' +
            'min_hash_length,' +
            'max_hash_length,' +
            'default_hash_length,' +
            'password_min_length,' +
            'password_max_length'
        const numbers = names.split(',')
        let numbersAsString = ""
        for (let i = 2; i < numbers.length + 2; i++) {
            numbersAsString += `$${i},`
        }
        numbersAsString = numbersAsString.substring(0, numbersAsString.length - 1);
        let settingValues = []

        for (let setting of Object.values(this.settings)) {
            settingValues.push(setting)
        }

        return {string: names, numbers: numbersAsString, settings: settingValues}
    }

}
