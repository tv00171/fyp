import BaseError from "./BaseError";
import {ResultRow} from "ts-postgres";

export default class ProjectModel {
    /**
     * The id that identifies the user in the DB
     */
    id: number;
    /**
     * The email associated with the account
     */
    name: string;

    /**
     * The user the project belongs to
     */
    dev_user: number

    constructor(fields: {id: number, name: string, dev_user: number}) {
        this.id = fields.id;
        this.name = fields.name;
        this.dev_user = fields.dev_user;

    }

    toJSON(){
        return {id: this.id, name:this.name, dev_user: this.dev_user}
    }

    static fromJSON(json: any){
        try{
            return new ProjectModel({id:json.id, name: json.email, dev_user: json.token});
        }catch (e) {
            throw new BaseError({error: e, errno:5});
        }
    }

    static fromDB(row: ResultRow){
        try{
            return new ProjectModel({id:+row.get("id").toString(), name: row.get("name").toString(), dev_user: +row.get("dev_user").toString()});
        }catch (e:any) {
            throw new BaseError({error: e.toString(), errno:5});
        }
    }

}
