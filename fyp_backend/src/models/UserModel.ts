import BaseError from "./BaseError";
import {ResultRow} from "ts-postgres";

export default class UserModel {
    /**
     * The id that identifies the user in the DB
     */
    id: number;
    /**
     * The email associated with the account
     */
    email: string;
    /**
     * The session token associated with the account
     */
    token: string;
    /**
     * The has representing the password of the user
     */
    hash: string;

    constructor(fields: { id: number, email: string, token?: string }) {
        this.email = fields.email;
        this.id = fields.id;
        this.token = fields.token;
    }

    toJSON() {
        return {id: this.id, email: this.email, token: this.token, hash: this.hash}
    }

    static fromJSON(json: any) {
        try {
            return new UserModel({id: json.id, email: json.email});
        } catch (e) {
            throw new BaseError({error: e, errno: 5});
        }
    }

    static fromDB(row: ResultRow) {
        try {
            return new UserModel({
                id: +row.get("id").toString(),
                email: row.get("email").toString(),
                token: row.get('token').toString()
            });
        } catch (e: any) {
            throw new BaseError({error: e.toString(), errno: 5});
        }
    }

}
