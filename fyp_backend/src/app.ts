import express from 'express';
import cors from "cors";

import routes from './routes/routes';
import auth from './routes/auth'
import projects from "./routes/projects";
import endUsers from "./routes/endUsers";
import authMiddleware from "./helpers/middleware";

class App {
    public app;

    constructor() {
        this.app = express();

        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.app.use(express.json());
        this.app.use(cors());
    }

    routes() {
        this.app.use(routes);
        this.app.use('/auth', auth);
        this.app.use('/projects', authMiddleware, projects);
        this.app.use('/endUsers', authMiddleware, endUsers);
    }
}

export default new App().app;
