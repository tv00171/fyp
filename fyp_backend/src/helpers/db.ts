import {Client, Result} from 'ts-postgres';
import BaseError from "../models/BaseError";

export default async function query(str: string, arr?: any) {
    const client = await connect();
    try {
        let result: Result;
        if (arr == null) {
            result = await client.query(
                str
            );
        } else {
            result = await client.query(str, arr);
        }
        return result;
    } catch (e) {
        throw new BaseError({error: e, errno: 8})
    } finally {
        await client.end();
    }
}

async function connect() {
    try {
        const client = new Client({database: "FYP_auth", user: "FYP_auth", password: "FYP_Password", port: 5434}); //TODO keep secure
        await client.connect();

        return client;
    } catch (e) {
        throw new BaseError({error: e, errno: 1});
    }
}
