import {NextFunction, Request, Response} from "express"
import User from "../models/UserModel";
import query from './../helpers/db';
import BaseError from "../models/BaseError";

/**
 * Checks the token is valid using the firebase authentication services
 * @returns
 */
async function authMiddleware(req: Request, res: Response, next: NextFunction) {
    let token = req.headers.authorization
    // Check the token is not null
    if (token == null) {
        return res.status(400).json({success: false, error: 'No token'})
    }

    let user;
    // Check if the token is valid
    try {
        // Get the user from the db with the same email as the one in firebase
        let userQuery = await query('SELECT * FROM users.dev_users WHERE token = $1', [token]);

        for (let row of userQuery) {
            user = User.fromDB(row)
        }

        if (user == null) {
            throw new BaseError({errno: 3, error: 'Token not valid', status: 401})
        }
        res.locals.user = user;
    } catch (error) {
        if (error instanceof BaseError) {
            return res.status(error.status).json(error.toJSON())
        }
        return res.status(500).json({success: false, error: error, errno: 6})
    }
    next();
}

export = authMiddleware;
