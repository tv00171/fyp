import argon2 from 'argon2'
import BaseError from "../models/BaseError";
import crypto from "crypto";

export async function hashPassword(password: string, settings?: { hashLength?: number }) {
    // Hash the password with argon2
    try {
        // Hash the password
        const passwordHash = await argon2.hash(password, settings);
        return passwordHash;
    } catch (e) {
        throw new BaseError({error: 'Error hashing password', status: 500, errno: 9})
    }

}

export async function verifyPassword(hashPassword: string, plainPassword: string) {
    if (!await argon2.verify(hashPassword, plainPassword)) {
        throw new BaseError({error: 'Invalid credentials', status: 401, errno: 7})
    }
    return true;
}

export function generateToken(length: number) {
    try {
        return crypto.randomBytes(length).toString('base64')
    } catch (e) {
        throw new BaseError({error: e, errno: 9})
    }
}
