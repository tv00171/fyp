import query from "../helpers/db";
import EndUserModel from "../models/EndUserModel";
import BaseError from "../models/BaseError";
import {generateToken, hashPassword} from "../helpers/authenticatonHelper";
import ProjectService from "./projectService";
import EndUserSettingsModel from "../models/EndUserSettingsModel";

export default class EndUserService {
    /**
     * Gets all the EndUsers in the DB
     */
    static async getAllEndUsers() {
        const allEndUsersQueryResult = await query('SELECT * FROM users.end_users')
        let allEndUsers: any[] = [];
        for (let res of allEndUsersQueryResult) {
            allEndUsers.push(EndUserModel.fromDB(res));
        }
        return allEndUsers;
    }

    /**
     * Get the information of an end user using the end user id which is unique across all projects
     * @param endUserId
     */
    static async getEndUser(endUserId: number) {
        const endUserQueryResult = await query('SELECT * FROM users.end_users WHERE id = $1', [endUserId]);
        if (endUserQueryResult.rows.length == 0) {
            throw new BaseError({errno: 13, status: 404, error: "User not found"})
        }
        for (let row of endUserQueryResult) {
            return EndUserModel.fromDB(row);
        }
    }


    /**
     * Gets all the endUsers of a project
     * @param projectId
     */
    static async getProjectEndUsers(projectId: number) {
        const projectEndUsersQueryResult = await query('SELECT * FROM users.end_users WHERE project_id = $1', [projectId])
        let projectEndUsers: any[] = [];
        for (let res of projectEndUsersQueryResult) {
            projectEndUsers.push(EndUserModel.fromDB(res));
        }
        return projectEndUsers;
    }

    /**
     * Change the settings of an endUser. The settings will have to follow the project overall settings
     * @param endUserId
     * @param settings
     */
    static async changeSettings(endUserId: number, settings: any) {
        // Get the projectId from the endUser
        const projectId = await EndUserService.getProjectIdFromEndUserId(endUserId);
        // Get the project settings
        const projectSettings = await ProjectService.getProjectSettings(projectId);
        const endUserSettings = await EndUserService.getEndUserSettings(endUserId);

        // Check the new settings are within the project security settings
        if (!projectSettings.settings.session_token.is_user_editable && settings.session_token_duration != endUserSettings.settings.session_token_duration) {
            throw new BaseError({error: "Session token is not editable", errno: 19})
        }

        if (!projectSettings.settings.refresh_token.is_user_editable && settings.refresh_token_duration != endUserSettings.settings.refresh_token_duration) {
            throw new BaseError({error: "Refresh token is not editable", errno: 19})
        }
        if (!withinRange(settings.session_token_duration, projectSettings.settings.session_token.min_duration, projectSettings.settings.session_token.max_duration)
            || !withinRange(settings.refresh_token_duration, projectSettings.settings.refresh_token.min_duration, projectSettings.settings.refresh_token.max_duration)) {
            throw new BaseError({error: "Settings are outside allowed bounds", errno: 18, status: 400})
        }
        if (!withinRange(settings.n_security_questions, projectSettings.settings.authentication_methods.security_questions.min_n_questions, projectSettings.settings.authentication_methods.security_questions.max_n_questions)) {
            throw new BaseError({error: "Security Questions are outside allowed bounds", errno: 18, status: 400})
        }
        await query('UPDATE users.end_user_settings SET session_token_duration = $1, refresh_token_duration = $2, n_security_questions = $3, use_security_questions = $4', [settings.session_token_duration, settings.refresh_token_duration, settings.n_security_questions, settings.use_security_questions])

        if (settings.security_questions != null) {
            // Delete all the existing security questions and add the new ones
            await query('DELETE FROM users.security_questions WHERE end_user_id = $1', [endUserId])

            for (let question of settings.security_questions) {
                await query('INSERT INTO users.security_questions(end_user_id, question, answer) VALUES ($1,$2,$3)', [endUserId, question.question, question.answer])
            }
        }

    }

    /**
     * Creates an endUser for a project using the project current settings
     * @param username
     * @param password
     * @param projectId
     */
    static async createEndUser(username: string, password: string, projectId: number, settings?: any) {
        let projectSettings = await ProjectService.getProjectSettings(projectId);
        // Check the username has the correct length
        if (username.length > projectSettings.settings.username.max_length || username.length < projectSettings.settings.username.min_length) {
            throw new BaseError({errno: 11, error: "Username too short or too long"});
        }
        // Check the username passes the regex
        if ((new RegExp(projectSettings.settings.username.regex)).test(username) == false) {
            throw new BaseError({error: "Username regex did not pass", errno: 11});
        }
        // Check the username passes the regex
        if ((new RegExp(projectSettings.settings.password.regex)).test(password) == false) {
            throw new BaseError({error: "Password regex did not pass", errno: 11});
        }
        // Check the password has the correct settings
        if (password.length < projectSettings.settings.password.min_length || password.length > projectSettings.settings.password.max_length) {
            throw new BaseError({errno: 10, error: "Password too short or too long"});
        }

        // Hash the password using the project settings
        const hashedPassword = await hashPassword(password, {hashLength: projectSettings.settings.password.hash_length})

        const session_token = generateToken(projectSettings.settings.session_token.length);
        const refresh_token = generateToken(projectSettings.settings.refresh_token.length);
        let session_token_date = new Date()
        session_token_date.setDate(session_token_date.getDate() + projectSettings.settings.session_token.default_duration)
        let refresh_token_date = new Date()
        refresh_token_date.setDate(refresh_token_date.getDate() + projectSettings.settings.refresh_token.default_duration)
        // At this point all the checks have passed so we insert the end_user into the db
        const createdEndUserQueryResult = await query('INSERT INTO users.end_users(project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) VALUES($1,$2,$3, $4, $5, $6, $7) RETURNING id', [projectId, username,
            hashedPassword, session_token, session_token_date, refresh_token, refresh_token_date]);
        let insertedEndUserId = createdEndUserQueryResult.rows[0][0];


        // Now create settings for this specific user
        await query(`INSERT INTO users.end_user_settings(end_user_id, refresh_token_duration,
                                                         session_token_duration)
                     VALUES ($1, $2,
                             $3)`, [insertedEndUserId, projectSettings.settings.password.hash_length, projectSettings.settings.refresh_token.default_duration, projectSettings.settings.session_token.default_duration]);

        //If the user included security questions also add them
        if (settings != null && settings.security_questions != null) {
            for (let question of settings.security_questions) {
                await query('INSERT INTO users.security_questions(end_user_id, question, answer) VALUES ($1,$2,$3)', [insertedEndUserId, question.question, question.answer])
            }
        }

        return await this.getEndUser(+insertedEndUserId);
    }

    /**
     * Gets the settings of an endUser and the current chosen values for them
     */
    static async getEndUserSettings(endUserId: number) {
        // Get the settings of the endUser
        const endUserSettingsQueryResponse = await query('SELECT * FROM users.end_user_settings WHERE end_user_id = $1', [endUserId]);
        for (let row of endUserSettingsQueryResponse) {
            return EndUserSettingsModel.fromDB(row);
        }
    }

    /**
     * Gets the security questions of and endUser from the DB
     * @param endUserId
     */
    static async getEndUserSecurityQuestions(endUserId: number) {
        const endUserSecurityQuestionsQueryResponse = await query('SELECT * FROM users.security_questions WHERE end_user_id = $1', [endUserId])
        let endUserSecurityQuestions = []
        for (let row of endUserSecurityQuestionsQueryResponse) {
            endUserSecurityQuestions.push({
                id: row.get('id'),
                question: row.get('question'),
                answer: row.get('answer')
            })
        }
        return endUserSecurityQuestions
    }

    /**
     * Get the projectId from an endUser id
     * @param endUserId
     */
    static async getProjectIdFromEndUserId(endUserId: number) {
        const projectIdQueryResult = await query('SELECT * FROM users.end_users WHERE id = $1', [endUserId])
        if (projectIdQueryResult.rows.length == 0) {
            throw new BaseError({error: "Project not found", errno: 13, status: 404});
        }
        for (let row of projectIdQueryResult) {
            return +row.get('project_id');
        }
    }

    /**
     * Get the endUserId from a username
     * @param endUserUsername
     */
    static async getEndUserIdFromUsername(endUserUsername: string, projectId: number) {
        const projectIdQueryResult = await query('SELECT * FROM users.end_users WHERE username = $1 AND project_id = $2', [endUserUsername, projectId])
        if (projectIdQueryResult.rows.length == 0) {
            throw new BaseError({error: "User not found", errno: 13, status: 404});
        }
        for (let row of projectIdQueryResult) {
            return +row.get('id');
        }
    }

    /**
     *  Authenticate an endUser with their session token and return the user if it is valid.
     * @param projectId
     * @param token
     */
    static async authenticateToken(projectId: number, token: string) {
        const tokenAuthenticationQueryResult = await query('SELECT * FROM users.end_users WHERE token = $1 AND project_id = $2', [token, projectId]);
        if (tokenAuthenticationQueryResult.rows.length == 0) {
            throw new BaseError({error: "Token not valid", status: 401, errno: 3});
        }
        let endUser;
        for (let row of tokenAuthenticationQueryResult) {
            endUser = EndUserModel.fromDB(row);
        }

        const currentDate = new Date();
        const expiryDate = new Date(Date.parse(endUser.token_expires))

        if (currentDate > expiryDate) {
            throw new BaseError({error: "Token has expired", errno: 21, status: 401})
        }
        return endUser;
    }

    /**
     * Set a new time for the tokens from the current date. Get the setting from the endUsers settings page
     * @param endUserId
     */
    static async restartTokensTimers(endUserId: number) {
        const endUserSettings = await EndUserService.getEndUserSettings(endUserId);
        let new_token_date = new Date()
        new_token_date.setDate(new_token_date.getDate() + endUserSettings.settings.session_token_duration)
        let new_refresh_token_date = new Date()
        new_refresh_token_date.setDate(new_refresh_token_date.getDate() + endUserSettings.settings.session_token_duration)

        await query('UPDATE users.end_users SET token_expires=  $1, refresh_token_expires = $2 WHERE id = $3', [new_token_date, new_refresh_token_date, endUserId])
    }

    static async refreshToken(refreshToken: string, projectId: number) {
        const refreshTokenQueryResult = await query('SELECT * FROM users.end_users WHERE refresh_token = $1 AND project_id = $2', [refreshToken, projectId])
        if (refreshTokenQueryResult.rows.length == 0) {
            throw new BaseError({errno: 3, status: 401, error: "Invalid refresh token"})
        }
        let endUser;
        for (let row of refreshTokenQueryResult) {
            endUser = EndUserModel.fromDB(row);
        }

        const currentDate = new Date();
        const expiryDate = new Date(Date.parse(endUser.refresh_token_expires))

        if (currentDate > expiryDate) {
            throw new BaseError({error: "Token has expired", errno: 21, status: 401})
        }

        const endUserSettings = await this.getEndUserSettings(endUser.id)
        const projectSettings = await ProjectService.getProjectSettings(endUser.projectId)
        // Generate new tokens and expirations dates
        const session_token = generateToken(projectSettings.settings.session_token.length);
        const refresh_token = generateToken(projectSettings.settings.refresh_token.length);
        let session_token_date = new Date()
        session_token_date.setDate(session_token_date.getDate() + endUserSettings.settings.session_token_duration)
        let refresh_token_date = new Date()
        refresh_token_date.setDate(refresh_token_date.getDate() + endUserSettings.settings.refresh_token_duration)

        await query('UPDATE users.end_users SET token= $1, token_expires = $2, refresh_token = $3, refresh_token_expires = $4 WHERE id = $5', [session_token, session_token_date, refresh_token, refresh_token_date, endUser.id])
        return {session_token: session_token, refresh_token: refresh_token}
    }

    static async deleteEndUser(username: string) {
        await query("DELETE FROM users.end_users WHERE username = $1", [username])
    }

}


/**
 * Check if a value is between a range of 2 values
 * @param value
 * @param min
 * @param max
 */
function withinRange(value: number, min: number, max: number) {
    if (value < min || value > max) {
        return false
    }
    return true;
}
