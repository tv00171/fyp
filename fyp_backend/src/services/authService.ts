import query from "../helpers/db";
import {Result} from "ts-postgres";
import BaseError from "../models/BaseError";
import UserModel from "../models/UserModel";

import {generateToken, hashPassword, verifyPassword} from "../helpers/authenticatonHelper";

export default class AuthService {
    /**
     * Authenticates the user using the email and password. Throws an error if the credentials are wrong.
     *
     * Returns the authenticated user
     */
    static async login(email: string, password: string) {

        // Find user and get hash
        let result: Result = await query('SELECT * FROM users.dev_users WHERE email = $1 LIMIT 1', [email]);
        let row;

        // Check there is a user with that email
        if (result.rows.length == 0) {
            throw new BaseError({error: 'User not found in the db', errno: 13, status: 400})
        }

        for (let res of result) {
            row = res;
        }
        const passwordHash: string = row.get("hash").toString();
        // Verify the password. If the passwords do not match it will throw a custom error and the code will not progress from here
        await verifyPassword(passwordHash, password)

        const user: UserModel = UserModel.fromDB(row);
        // Compare hashes
        return user;
    }

    /**
     * Register the user to the db using the email and password. The password is hashed.
     *
     * Returns the created user
     */
    static async register(email: string, password: string) {
        // Hash password
        const hashedPassword = await hashPassword(password);

        // Create token
        const token = generateToken(100);

        // Put user in db
        await query('INSERT INTO users.dev_users(email, hash,token) VALUES($1,$2,$3) RETURNING id', [email, hashedPassword, token]);

        // Return new user
        let createdUser = await query('SELECT * FROM users.dev_users WHERE email = $1', [email]);
        let user;
        for (let row of createdUser) {
            user = row;
        }
        return UserModel.fromDB(user);
    }

    static async remove(email: string) {
        await query('DELETE FROM users.dev_users WHERE email = $1', [email])
        return true;
    }


}
