import query from "../helpers/db";
import ProjectModel from "../models/ProjectModel";
import BaseError from "../models/BaseError";
import ProjectSettingsModel from "../models/ProjectSettingsModel";
import EndUserModel from "../models/EndUserModel";
import {verifyPassword} from "../helpers/authenticatonHelper";
import EndUserService from "./endUserService";

export default class ProjectService {
    /**
     * Get all the projects in the DB
     */
    static async getAllProjects(user_id: number) {
        const allProjectsResult = await query("SELECT * FROM projects.projects WHERE dev_user = $1", [user_id]);
        let allProjects: any[] = [];
        for (let res of allProjectsResult) {
            allProjects.push(ProjectModel.fromDB(res));
        }
        return allProjects;
    }

    static async getProject(projectId: number) {
        const projectsQueryResult = await query("SELECT * FROM projects.projects WHERE id = $1", [projectId]);
        if (projectsQueryResult.rows.length == 0) {
            throw new BaseError({errno: 13, status: 404, error: "Project not found"})
        }
        for (let res of projectsQueryResult) {
            return ProjectModel.fromDB(res);
        }
    }

    /**
     * Get all the settings of a project
     * @param projectId
     */
    static async getProjectSettings(projectId: number) {
        let projectSettingsObject = new ProjectSettingsModel({projectId})
        // Since the settings are divided in different tables, this makes a call to each table to get the data

        // Initially this was going to be a JOIN table, but since the values in different tables have the same names, it
        // would require me to manually specify each column to have the appropriate name, which would take way too long.
        // Instead, I make a call to each table and parse the values. It is less efficient but more organized and easier to understand
        projectSettingsObject.settings.username = await this.getUsernameSettings(projectId)
        projectSettingsObject.settings.password = await this.getPasswordSettings(projectId);
        projectSettingsObject.settings.refresh_token = await this.getRefreshTokenSettings(projectId);
        projectSettingsObject.settings.session_token = await this.getSessionTokenSettings(projectId);

        // Get the authentication methods available and their current state in the project
        projectSettingsObject.settings.authentication_methods.security_questions = await this.getSecurityQuestionsSettings(projectId)

        return projectSettingsObject;
    }

    /**
     * Get the security questions settings of a project from the DB
     * @param projectId
     */
    static async getSecurityQuestionsSettings(projectId: number) {
        const securityQuestionsSettingsQueryResult = await query('SELECT * FROM settings.security_questions_settings WHERE project_id = $1', [projectId])
        for (let row of securityQuestionsSettingsQueryResult) {
            return ProjectSettingsModel.securityQuestionsFromDB(row)
        }
    }

    /**
     * Get the username settings of a project
     * @param projectId
     */
    static async getUsernameSettings(projectId: number) {
        const usernameSettingsResponse = await query('SELECT * FROM settings.username_settings WHERE project_id = $1', [projectId])
        if (usernameSettingsResponse.rows.length == 0) {
            throw new BaseError({error: "Username settings not found", errno: 13, status: 404});
        }
        for (let res of usernameSettingsResponse) {
            return ProjectSettingsModel.usernameSettingsFromDB(res)
        }
    }

    /**
     * Get the password settings of a project from the DB
     * @param projectId
     */
    static async getPasswordSettings(projectId: number) {
        const passwordSettingsResponse = await query('SELECT * FROM settings.password_settings WHERE project_id = $1', [projectId])
        if (passwordSettingsResponse.rows.length == 0) {
            throw new BaseError({error: "Password settings not found", errno: 13, status: 404});
        }
        for (let res of passwordSettingsResponse) {
            return ProjectSettingsModel.passwordSettingsFromDB(res)
        }
    }

    /**
     * Get the password settings of a project from the DB
     * @param projectId
     */
    static async getRefreshTokenSettings(projectId: number) {
        const refreshTokenSettingsResponse = await query('SELECT * FROM settings.refresh_token_settings WHERE project_id = $1', [projectId])
        for (let res of refreshTokenSettingsResponse) {
            return ProjectSettingsModel.refreshTokenSettingsFromDB(res)
        }
    }

    /**
     * Get the password settings of a project from the DB
     * @param projectId
     */
    static async getSessionTokenSettings(projectId: number) {
        const sessionTokenSettingsResponse = await query('SELECT * FROM settings.session_token_settings WHERE project_id = $1', [projectId])
        for (let res of sessionTokenSettingsResponse) {
            return ProjectSettingsModel.sessionTokenSettingsFromDB(res)
        }
    }


    /**
     * Create a project in the DB
     * @param name
     * @param settings
     * @param user_id
     */
    static async createProject(name: string, user_id: number, settings?: any,) {
        // Create the project
        const insertedProjectQueryResult = await query("INSERT INTO projects.projects(name, dev_user) VALUES ($1,$2) RETURNING id", [name, user_id])
        let insertedProjectId = insertedProjectQueryResult.rows[0][0];
        // If the user provided any settings then add them to the db. Since we want to use the default settings from the db
        // where the user did not provide custom settings, we only need to add to the query the parameters that have value.

        // Since the project settings are divided into different tables we need to make multiple calls to the DB to create all
        // the project settings.
        if (settings != undefined) {
            const usernameSettings = getKeysOfJSON(settings.username)
            const passwordSettings = getKeysOfJSON(settings.password)
            const refreshToken = getKeysOfJSON(settings.refresh_token)
            const sessionToken = getKeysOfJSON(settings.session_token)
            await query(`INSERT INTO settings.username_settings(project_id, ${usernameSettings.keys})
                         VALUES ($1, ${usernameSettings.numbers})`, [insertedProjectId, ...usernameSettings.values])
            await query(`INSERT INTO settings.password_settings(project_id, ${passwordSettings.keys})
                         VALUES ($1, ${passwordSettings.numbers})`, [insertedProjectId, ...passwordSettings.values])
            await query(`INSERT INTO settings.refresh_token_settings(project_id, ${refreshToken.keys})
                         VALUES ($1, ${refreshToken.numbers})`, [insertedProjectId, ...refreshToken.values])
            await query(`INSERT INTO settings.session_token_settings(project_id, ${sessionToken.keys})
                         VALUES ($1, ${sessionToken.numbers})`, [insertedProjectId, ...sessionToken.values])
        } else { // If there are not custom settings just create the table with all the default values
            await query(`INSERT INTO settings.username_settings(project_id)
                         VALUES ($1)`, [insertedProjectId])
            await query(`INSERT INTO settings.password_settings(project_id)
                         VALUES ($1)`, [insertedProjectId])
            await query(`INSERT INTO settings.refresh_token_settings(project_id)
                         VALUES ($1)`, [insertedProjectId])
            await query(`INSERT INTO settings.session_token_settings(project_id)
                         VALUES ($1)`, [insertedProjectId])
            await query('INSERT INTO settings.security_questions_settings(project_id) VALUES($1)', [insertedProjectId])
        }

        //Return the created project
        const createdproject = await query('SELECT * FROM projects.projects WHERE id = $1', [insertedProjectId])
        let project;
        for (let row of createdproject) {
            project = row;
        }
        return ProjectModel.fromDB(project);
    }

    /**
     * Change the security settings of an already existing project
     * @param projectId
     * @param settings
     */
    static async changeProjectSettings(projectId: number, settings: any) {

        // Here there will be an error if the fields does not exist in the db, so instead of throwing a db error which could be missleading
        // it will throw a wrong parameter error
        try {
            // Go through each security table in the db and make the changes if it is not empty
            if (settings.username != null) {
                const usernameNewSettings = getStringForUpdateSQL(settings.username);
                await query(`UPDATE settings.username_settings
                             SET ${usernameNewSettings.string}
                             WHERE project_id = $1`, [projectId, ...usernameNewSettings.values])
            }
            if (settings.password != null) {
                const passwordNewSettings = getStringForUpdateSQL(settings.password);
                await query(`UPDATE settings.password_settings
                             SET ${passwordNewSettings.string}
                             WHERE project_id = $1`, [projectId, ...passwordNewSettings.values])
            }
            if (settings.session_token != null) {
                const sessionTokenNewSettings = getStringForUpdateSQL(settings.session_token);
                await query(`UPDATE settings.session_token_settings
                             SET ${sessionTokenNewSettings.string}
                             WHERE project_id = $1`, [projectId, ...sessionTokenNewSettings.values])
            }
            if (settings.refresh_token != null) {
                const refreshTokenNewSettings = getStringForUpdateSQL(settings.refresh_token);
                await query(`UPDATE settings.refresh_token_settings
                             SET ${refreshTokenNewSettings.string}
                             WHERE project_id = $1`, [projectId, ...refreshTokenNewSettings.values])
            }
            if (settings.authentication_methods != null && settings.authentication_methods.security_questions != null) {
                const securityQuestionsNewSettings = getStringForUpdateSQL(settings.authentication_methods.security_questions);
                await query(`UPDATE settings.security_questions_settings
                             SET ${securityQuestionsNewSettings.string}
                             WHERE project_id = $1`, [projectId, ...securityQuestionsNewSettings.values])
                if (settings.authentication_methods.security_questions.is_user_editable == false) {
                    await query('UPDATE users.end_user_settings SET use_security_questions = $1, n_security_questions =$2 FROM users.end_user_settings s LEFT JOIN users.end_users e ON e.id = s.end_user_id WHERE e.project_id = $3', [settings.authentication_methods.security_questions.is_active, settings.authentication_methods.security_questions.n_questions, projectId])
                }
            }
        } catch (e) {
            throw new BaseError({errno: 5, error: "Invalid security fields", status: 400})
        }
    }

    static async loginProjectUsingPassword(username: string, password: string, projectId: number) {
        const loginQueryResult = await query('SELECT * FROM users.end_users WHERE username = $1 AND project_id =$2', [username, projectId])
        if (loginQueryResult.rows.length == 0) {
            throw new BaseError({error: "User not found", errno: 7, status: 401})
        }
        let endUser: EndUserModel;
        for (let row of loginQueryResult) {
            endUser = EndUserModel.fromDB(row)
        }
        await verifyPassword(endUser.password, password);

        return endUser.toJson();
    }

    /**
     * Logs in an endUser using security questions
     * @param username
     * @param securityQuestionsAnswers
     */
    static async loginProjectUsingSecurityQuestions(username: string, securityQuestionsAnswers: any, projectId: number) {
        const endUserId = await EndUserService.getEndUserIdFromUsername(username, projectId);
        const securityQuestions = await EndUserService.getEndUserSecurityQuestions(endUserId);
        const endUserSettings = await EndUserService.getEndUserSettings(endUserId);

        if (endUserSettings.settings.use_security_questions == false) {
            throw new BaseError({errno: 22, error: "Security questions are not enabled for this end user", status: 401})
        }

        // Check if all the security questions answers are right
        for (let securityQuestion of securityQuestions) {
            let answer = null
            // Get the security questions answer that has the same id as the question
            for (let securityQuestionAnswer of securityQuestionsAnswers) {
                if (securityQuestionAnswer.id == securityQuestion.id) {
                    answer = securityQuestionAnswer.answer
                }
            }
            // If the answer is still null it means it didn't find it and therefore it is missing
            if (answer == null) {
                throw new BaseError({errno: 5, error: "Missing security questions answer", status: 401})
            }
            // Compare if the answers are the same
            if (securityQuestion.answer != answer) {
                throw new BaseError({errno: 20, error: "Security Questions Answers don't match", status: 401})
            }
        }
        // If it gets here it means all the security questions where answered right, so we log the user in
        const user = await EndUserService.getEndUser(endUserId)
        return user.toJson();
    }

    static async deleteProject(name: string) {

        // Delete the project, it should also delete all the dependencies
        await query('DELETE FROM projects.projects WHERE name = $1', [name])

    }
}

/**
 * Get all the keys, values and numbers of a json
 * @param json
 */
function getKeysOfJSON(json: any) {
    let keys = []
    let numbers = []
    let values = []

    // Logic to make db use default values
    for (let [i, v] of Object.keys(json).entries()) {
        keys.push(v)
        numbers.push(`$${i + 2}`)
        values.push(json[v])
    }

    return {keys, numbers, values}
}

function getStringForUpdateSQL(json: any) {
    let keys = []
    let values = []
    let string = ''
    // Logic to make db use default values
    for (let [i, v] of Object.keys(json).entries()) {
        if (i != 0) {
            string += ','
        }
        string += `${v} = $${i + 2}`
        keys.push(v)
        values.push(json[v])
    }

    return {string, values}
}
