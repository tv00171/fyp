// @ts-ignore
import request from "supertest"
import endUserService from "../../src/services/endUserService";
import EndUserService from "../../src/services/endUserService";
import ProjectService from "../../src/services/projectService";

/**
 * Test the developer authentication service which connects to the database and contains all the business logic.
 */
let projectId: number;
describe('End User Service', function () {
    beforeAll(async () => {
        projectId = (await ProjectService.createProject("TestProject3", 0)).id;
        await ProjectService.changeProjectSettings(projectId, {session_token: {is_user_editable: true}})
    })
    afterAll(async () => {
        await ProjectService.deleteProject("TestProject3");
        await endUserService.deleteEndUser("test1");
        await endUserService.deleteEndUser("test2");
        await endUserService.deleteEndUser("test3");
        await endUserService.deleteEndUser("test4");
        await endUserService.deleteEndUser("test5");
        await endUserService.deleteEndUser("test6");

    })

    it('create end user in project', async () => {
        const endUser = await endUserService.createEndUser("test1", "Password_123", projectId);
        expect(endUser.username).toEqual('test1');
    });
    it('change end user settings', async () => {
        const endUser = await endUserService.createEndUser("test2", "Password_123", projectId);
        const endUserSettings = await endUserService.getEndUserSettings(endUser.id)
        endUserSettings.settings.session_token_duration = 4;
        await endUserService.changeSettings(endUser.id, endUserSettings.settings)
        const newEndUserSettings = await endUserService.getEndUserSettings(endUser.id)
        expect(newEndUserSettings.settings.session_token_duration).toEqual(4);
    });
    it('change end user settings outside project settings bound', async () => {
        const endUser = await endUserService.createEndUser("test3", "Password_123", projectId);
        const endUserSettings = await endUserService.getEndUserSettings(endUser.id)
        endUserSettings.settings.session_token_duration = 400;
        try {
            await endUserService.changeSettings(endUser.id, endUserSettings.settings)
        } catch (e) {
            expect(e.errno).toEqual(18)
        }
    });
    it('login end-user with valid password', async () => {
        const endUser = await endUserService.createEndUser("test5", 'Password_123', projectId);
        let session = await ProjectService.loginProjectUsingPassword('test5', 'Password_123', projectId);
        expect(session).not.toBeNull();
    });
    it('login end-user with invalid password', async () => {
        const endUser = await endUserService.createEndUser("test6", 'Password_123', projectId);
        try {
            await ProjectService.loginProjectUsingPassword('test6', 'Password_1234', projectId)
        } catch (e) {
            expect(e.status).toEqual(401)
        }
    });
    it('login end-user with valid security questions', async () => {
        const endUser = await endUserService.createEndUser("test7", 'Password_123', projectId);
        let projectSettings = await ProjectService.getProjectSettings(projectId)
        projectSettings.settings.authentication_methods.security_questions.is_active = true;
        await ProjectService.changeProjectSettings(projectId, projectSettings.settings)
        let userSettings = await endUserService.getEndUserSettings(endUser.id);
        await endUserService.changeSettings(endUser.id, {
            ...userSettings.settings, security_questions: [{
                question: "1+1?",
                answer: "2"
            }]
        })
        let sec_questions = await endUserService.getEndUserSecurityQuestions(endUser.id)
        let securityQuestions = await EndUserService.getEndUserSecurityQuestions(endUser.id)
        console.log(securityQuestions)
        let session = await ProjectService.loginProjectUsingSecurityQuestions('test6', [{
            id: sec_questions[0].id,
            answer: '2'
        }], projectId);

        expect(session.projectId).toEqual(projectId)
    });
    it('remove end user', async () => {
        const endUser = await endUserService.createEndUser("test4", 'Password_123', projectId);
        await endUserService.deleteEndUser('test4')
        try {
            await endUserService.getEndUser(endUser.id)
        } catch (e) {
            expect(e.status).toEqual(404)
        }
    });
});
