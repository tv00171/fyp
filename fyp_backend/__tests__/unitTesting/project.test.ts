// @ts-ignore
import request from "supertest"
import AuthService from "../../src/services/authService";
import ProjectService from "../../src/services/projectService";

/**
 * Test the developer authentication service which connects to the database and contains all the business logic.
 */
describe('Project Service', function () {
    beforeAll(async () => {
        await AuthService.register('test2-3@gmail.com', '123')
    })
    afterAll(async () => {
        await AuthService.remove('test2-3@gmail.com')
        await ProjectService.deleteProject('TestProject')
        await ProjectService.deleteProject('TestProject2')

    })

    it('create project without special settings', async () => {
        const project = await ProjectService.createProject("TestProject", 0)
        expect(project.name).toEqual('TestProject');
    });
    it('change project settings', async () => {
        const project = await ProjectService.createProject("TestProject2", 0);
        await ProjectService.changeProjectSettings(project.id, {username: {max_length: 10}})
        const settings = await ProjectService.getProjectSettings(project.id)
        expect(project.name).toEqual('TestProject2');
        expect(settings.settings.username.max_length).toEqual(10)
    });
});
