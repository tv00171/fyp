// @ts-ignore
import request from "supertest"
import AuthService from "../../src/services/authService";

/**
 * Test the developer authentication service which connects to the database and contains all the business logic.
 */
describe('Authentication Service', function () {
    beforeAll(async () => {
        await AuthService.register('test@gmail.com', '123')

    })
    afterAll(async () => {
        await AuthService.remove('test@gmail.com')
        await AuthService.remove('test2@gmail.com')
    })

    it('login using valid email and password ', async () => {
        const user = await AuthService.login('test@gmail.com', '123')
        expect(user.email).toEqual('test@gmail.com');
    });
    it('try login using invalid email and password ', async () => {
        try {
            await AuthService.login('test@gmail.com', '12345')
        } catch (e) {
            expect(e.errno).toEqual(7)
        }
    });
    it('create dev account ', async () => {
        const user = await AuthService.register('test2@gmail.com', '123')
        expect(user.email).toEqual("test2@gmail.com")
    });
});
