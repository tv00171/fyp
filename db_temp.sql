--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

-- Started on 2023-05-14 20:21:30 BST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16414)
-- Name: projects; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA projects;


ALTER SCHEMA projects OWNER TO postgres;

--
-- TOC entry 7 (class 2615 OID 16536)
-- Name: settings; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA settings;


ALTER SCHEMA settings OWNER TO postgres;

--
-- TOC entry 5 (class 2615 OID 16399)
-- Name: users; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA users;


ALTER SCHEMA users OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 219 (class 1259 OID 16416)
-- Name: projects; Type: TABLE; Schema: projects; Owner: postgres
--

CREATE TABLE projects.projects (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    dev_user integer NOT NULL
);


ALTER TABLE projects.projects OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16415)
-- Name: Projects_id_seq; Type: SEQUENCE; Schema: projects; Owner: postgres
--

ALTER TABLE projects.projects ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME projects."Projects_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 229 (class 1259 OID 16568)
-- Name: password_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.password_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    regex character varying(100) DEFAULT '.*'::character varying NOT NULL,
    min_length integer DEFAULT 5 NOT NULL,
    max_length integer DEFAULT 20 NOT NULL,
    hash_length integer DEFAULT 16 NOT NULL
);


ALTER TABLE settings.password_settings OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16567)
-- Name: password_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.password_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.password_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 227 (class 1259 OID 16554)
-- Name: refresh_token_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.refresh_token_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    length integer DEFAULT 16 NOT NULL,
    default_duration integer DEFAULT 14 NOT NULL,
    max_duration integer DEFAULT 32 NOT NULL,
    min_duration integer DEFAULT 7 NOT NULL
);


ALTER TABLE settings.refresh_token_settings OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16553)
-- Name: refresh_token_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.refresh_token_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.refresh_token_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 233 (class 1259 OID 24607)
-- Name: security_questions_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.security_questions_settings (
    id integer NOT NULL,
    n_questions integer DEFAULT 1 NOT NULL,
    project_id integer NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    min_n_questions integer DEFAULT 1 NOT NULL,
    max_n_questions integer DEFAULT 5 NOT NULL
);


ALTER TABLE settings.security_questions_settings OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 24606)
-- Name: security_questions_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.security_questions_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.security_questions_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 225 (class 1259 OID 16538)
-- Name: session_token_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.session_token_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    is_user_editable boolean DEFAULT false NOT NULL,
    length integer DEFAULT 16 NOT NULL,
    default_duration integer DEFAULT 14 NOT NULL,
    max_duration integer DEFAULT 28 NOT NULL,
    min_duration integer DEFAULT 1 NOT NULL
);


ALTER TABLE settings.session_token_settings OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16537)
-- Name: session_token_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.session_token_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.session_token_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 231 (class 1259 OID 16583)
-- Name: username_settings; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.username_settings (
    id integer NOT NULL,
    project_id integer NOT NULL,
    regex character varying(100) DEFAULT '.*'::character varying NOT NULL,
    min_length integer DEFAULT 5 NOT NULL,
    max_length integer DEFAULT 30 NOT NULL
);


ALTER TABLE settings.username_settings OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16582)
-- Name: username_settings_id_seq; Type: SEQUENCE; Schema: settings; Owner: postgres
--

ALTER TABLE settings.username_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME settings.username_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 16401)
-- Name: dev_users; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.dev_users (
    id integer NOT NULL,
    email character varying(50) NOT NULL,
    token character varying(255),
    hash character varying(255)
);


ALTER TABLE users.dev_users OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16400)
-- Name: dev_users_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.dev_users ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME users.dev_users_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 1000
    CACHE 1
    CYCLE
);


--
-- TOC entry 222 (class 1259 OID 16473)
-- Name: end_user_settings; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.end_user_settings (
    id integer NOT NULL,
    end_user_id integer NOT NULL,
    session_token_duration integer NOT NULL,
    refresh_token_duration integer NOT NULL,
    use_security_questions boolean DEFAULT false NOT NULL,
    n_security_questions integer DEFAULT 1 NOT NULL
);


ALTER TABLE users.end_user_settings OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16490)
-- Name: end_user_settings_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.end_user_settings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.end_user_settings_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 221 (class 1259 OID 16427)
-- Name: end_users; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.end_users (
    id integer NOT NULL,
    project_id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(500) NOT NULL,
    token character varying(256),
    token_expires timestamp without time zone,
    refresh_token character varying(500),
    refresh_token_expires timestamp without time zone
);


ALTER TABLE users.end_users OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16426)
-- Name: end_users_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.end_users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.end_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE
);


--
-- TOC entry 235 (class 1259 OID 24622)
-- Name: security_questions; Type: TABLE; Schema: users; Owner: postgres
--

CREATE TABLE users.security_questions (
    id integer NOT NULL,
    question character varying(100) NOT NULL,
    answer character varying(100) NOT NULL,
    end_user_id integer
);


ALTER TABLE users.security_questions OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 24621)
-- Name: security_questions_id_seq; Type: SEQUENCE; Schema: users; Owner: postgres
--

ALTER TABLE users.security_questions ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME users.security_questions_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3684 (class 0 OID 16416)
-- Dependencies: 219
-- Data for Name: projects; Type: TABLE DATA; Schema: projects; Owner: postgres
--

COPY projects.projects (id, name, dev_user) FROM stdin;
57	Project 1	9
60	Project 3	9
\.


--
-- TOC entry 3694 (class 0 OID 16568)
-- Dependencies: 229
-- Data for Name: password_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.password_settings (id, project_id, regex, min_length, max_length, hash_length) FROM stdin;
0	57	.*	2	20	16
3	60	.*	5	20	16
\.


--
-- TOC entry 3692 (class 0 OID 16554)
-- Dependencies: 227
-- Data for Name: refresh_token_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.refresh_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) FROM stdin;
0	57	t	16	14	32	7
3	60	f	16	14	32	7
\.


--
-- TOC entry 3698 (class 0 OID 24607)
-- Dependencies: 233
-- Data for Name: security_questions_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.security_questions_settings (id, n_questions, project_id, is_active, is_user_editable, min_n_questions, max_n_questions) FROM stdin;
0	3	57	t	t	1	3
1	1	60	t	f	1	2
\.


--
-- TOC entry 3690 (class 0 OID 16538)
-- Dependencies: 225
-- Data for Name: session_token_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.session_token_settings (id, project_id, is_user_editable, length, default_duration, max_duration, min_duration) FROM stdin;
0	57	t	20	14	28	1
3	60	f	100	14	28	1
\.


--
-- TOC entry 3696 (class 0 OID 16583)
-- Dependencies: 231
-- Data for Name: username_settings; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.username_settings (id, project_id, regex, min_length, max_length) FROM stdin;
0	57	.*	2	30
3	60	.*	5	30
\.


--
-- TOC entry 3682 (class 0 OID 16401)
-- Dependencies: 217
-- Data for Name: dev_users; Type: TABLE DATA; Schema: users; Owner: postgres
--

COPY users.dev_users (id, email, token, hash) FROM stdin;
0	afda	\N	\N
2	afdf	\N	\N
9	tginkel1@gmail.com	LMXJNdlfV4upNwp0FqEhgd/J67n2t3z4LwIzZgFZdYPVL9aLCnFVPgDd/k6d/Mk7V3ayFCNVB4g2VOWdyVyFZzhZfeyP+Doyt3ed9y11j4JWx/xEwCjFBoE5NHqTT+r+35+0EQ==	$argon2id$v=19$m=65536,t=3,p=4$HZCBtu9YbZtsanL+I5RLlg$59AlrbvZ6Z3YETCA+Jw+heclu0umSTdYualh+HZR8Jk
190	tginkel4@gmail.com	hlcS9tweAxasoVGqDt0yj672wMxUYzpSCFaplNfCLYayhSkVJdotZKbRhj4m4nwXEhgQxDja2DQ+138fgfiv/DXa9zkFleVxhqDKjzfNnyZKcnItfrHEtR7Mf67fMz9SIs1dsw==	$argon2id$v=19$m=65536,t=3,p=4$t9miyrxfWwfG+gQgafrFDA$KJUg5jgYlgvpJBhmncEw4p1H3H770jUwIZMus3Z+dcQ
191	123@gmail.com	qCGuUAyMpDUNwNT9GE1B6xSvG0izLltRLx92FyeQrC9T2Kl+rS3N9aV75nPg/VSxaQm4rrsWA3Unfk+RmsqLgsiDfeF1PKd2LRZMtrmG/pfazE0ualF3MMUJavRfxIU5IPjG/Q==	$argon2id$v=19$m=65536,t=3,p=4$MWWliYkkTGQy56Kt+ccxWw$gvV0L+0MtZdfY7xt9ZI54bjsx/D6Q/aFz+N7c0xkgYw
193	1234@gmail.com	tTXAK+qPZ3MTxqYfCQvj7MvOQpT/n5tFm0nfXcLQ54/fQC50AueYN4mZ7jYrdvT1cl9Ct2UQttU5OxXzIzbnvbpt9zZShk6b9+yfQm4RheQUBpY/zCoNXPaF0F5PbpDwTqQhdA==	$argon2id$v=19$m=65536,t=3,p=4$viCSKaiD8GdB9tyQJU7Lpg$qbDDPTSgs4dv9nDGRi3OUUMmlXCP7ncq1eFFnhg7Ve8
\.


--
-- TOC entry 3687 (class 0 OID 16473)
-- Dependencies: 222
-- Data for Name: end_user_settings; Type: TABLE DATA; Schema: users; Owner: postgres
--

COPY users.end_user_settings (id, end_user_id, session_token_duration, refresh_token_duration, use_security_questions, n_security_questions) FROM stdin;
15	32	4	16	t	1
17	36	4	16	t	1
18	37	4	16	t	1
31	50	4	16	t	1
32	51	4	16	t	1
\.


--
-- TOC entry 3686 (class 0 OID 16427)
-- Dependencies: 221
-- Data for Name: end_users; Type: TABLE DATA; Schema: users; Owner: postgres
--

COPY users.end_users (id, project_id, username, password, token, token_expires, refresh_token, refresh_token_expires) FROM stdin;
36	60	toni321	$argon2id$v=19$m=65536,t=3,p=4$I3AiVt2j+ltXIO2VUiGg9g$qfEHkwJDgr6W4D5NMFOU8Q	token	\N	\N	\N
37	60	toni2	$argon2id$v=19$m=65536,t=3,p=4$CQYUi/1V3sUyc1TcMbzWfg$waW4FYGZMNTzMkNx4MnXdw	token	\N	\N	\N
32	57	toni123	$argon2id$v=19$m=65536,t=3,p=4$bWQytV0T4nj3hdC2mJkA0A$NMTGHzejK0xc4VX2tvkfVA	a	2023-05-18 16:58:02.882	\N	2023-05-18 16:58:02.882
50	60	test3	$argon2id$v=19$m=65536,t=3,p=4$GRO36zieF+fbsX5kh6uTRQ$oVewb5B6LZv0UfKCcMVDFQ	qPcHbY7IGtuE4d2ciVGTWV946C7tOm86dX6dK9fJO5SPO+5PRtT9tCbUiq0jdG0QfwgQ8XMSHFcMJiSlJoz5XJcMMZspKwTp+BMVevsAwK0CPdwMFChYJUykq9/CHHtBDz1y9w==	2023-02-14 17:35:22.19	\N	\N
51	60	test3	$argon2id$v=19$m=65536,t=3,p=4$h9c+vgYYUzlCgqdnq7RKfg$kAWoArkGUKhsvbVLLt2v2g	G103Aw8FqilZeJkF+mNoea1uJhrhLYX1x/LgU9vvZXQ0I9b1T87VVP58XuZOegNutJSE+/nCN2yugahM8u00UN8a/1NjrnXWy2xhm+c/ZTME/u73nVU+U2ExlEuwwE1HVeMXdw==	2023-03-14 18:42:05.14	fvs6UCgtmWX21KlXlv6D0g==	2023-03-16 18:42:05.14
\.


--
-- TOC entry 3700 (class 0 OID 24622)
-- Dependencies: 235
-- Data for Name: security_questions; Type: TABLE DATA; Schema: users; Owner: postgres
--

COPY users.security_questions (id, question, answer, end_user_id) FROM stdin;
44	Hello	Bye	36
49	Yes	No	37
61	a	aa	32
62	b	bb	32
\.


--
-- TOC entry 3706 (class 0 OID 0)
-- Dependencies: 218
-- Name: Projects_id_seq; Type: SEQUENCE SET; Schema: projects; Owner: postgres
--

SELECT pg_catalog.setval('projects."Projects_id_seq"', 178, true);


--
-- TOC entry 3707 (class 0 OID 0)
-- Dependencies: 228
-- Name: password_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.password_settings_id_seq', 120, true);


--
-- TOC entry 3708 (class 0 OID 0)
-- Dependencies: 226
-- Name: refresh_token_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.refresh_token_settings_id_seq', 120, true);


--
-- TOC entry 3709 (class 0 OID 0)
-- Dependencies: 232
-- Name: security_questions_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.security_questions_id_seq', 118, true);


--
-- TOC entry 3710 (class 0 OID 0)
-- Dependencies: 224
-- Name: session_token_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.session_token_settings_id_seq', 120, true);


--
-- TOC entry 3711 (class 0 OID 0)
-- Dependencies: 230
-- Name: username_settings_id_seq; Type: SEQUENCE SET; Schema: settings; Owner: postgres
--

SELECT pg_catalog.setval('settings.username_settings_id_seq', 120, true);


--
-- TOC entry 3712 (class 0 OID 0)
-- Dependencies: 216
-- Name: dev_users_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.dev_users_id_seq', 193, true);


--
-- TOC entry 3713 (class 0 OID 0)
-- Dependencies: 223
-- Name: end_user_settings_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.end_user_settings_id_seq', 80, true);


--
-- TOC entry 3714 (class 0 OID 0)
-- Dependencies: 220
-- Name: end_users_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.end_users_id_seq', 99, true);


--
-- TOC entry 3715 (class 0 OID 0)
-- Dependencies: 234
-- Name: security_questions_id_seq; Type: SEQUENCE SET; Schema: users; Owner: postgres
--

SELECT pg_catalog.setval('users.security_questions_id_seq', 62, true);


--
-- TOC entry 3515 (class 2606 OID 16420)
-- Name: projects Projects_pkey; Type: CONSTRAINT; Schema: projects; Owner: postgres
--

ALTER TABLE ONLY projects.projects
    ADD CONSTRAINT "Projects_pkey" PRIMARY KEY (id);


--
-- TOC entry 3525 (class 2606 OID 16575)
-- Name: password_settings password_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.password_settings
    ADD CONSTRAINT password_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3523 (class 2606 OID 16664)
-- Name: refresh_token_settings refresh_token_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.refresh_token_settings
    ADD CONSTRAINT refresh_token_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3529 (class 2606 OID 24611)
-- Name: security_questions_settings security_questions_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.security_questions_settings
    ADD CONSTRAINT security_questions_pkey PRIMARY KEY (id);


--
-- TOC entry 3521 (class 2606 OID 16547)
-- Name: session_token_settings session_token_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.session_token_settings
    ADD CONSTRAINT session_token_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3527 (class 2606 OID 16590)
-- Name: username_settings username_settings_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.username_settings
    ADD CONSTRAINT username_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3511 (class 2606 OID 16413)
-- Name: dev_users dev_users_email_unique; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.dev_users
    ADD CONSTRAINT dev_users_email_unique UNIQUE (email);


--
-- TOC entry 3513 (class 2606 OID 16405)
-- Name: dev_users dev_users_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.dev_users
    ADD CONSTRAINT dev_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3519 (class 2606 OID 16513)
-- Name: end_user_settings end_user_settings_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_user_settings
    ADD CONSTRAINT end_user_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3517 (class 2606 OID 16431)
-- Name: end_users end_users_pkey; Type: CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_users
    ADD CONSTRAINT end_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3530 (class 2606 OID 16421)
-- Name: projects dev_user; Type: FK CONSTRAINT; Schema: projects; Owner: postgres
--

ALTER TABLE ONLY projects.projects
    ADD CONSTRAINT dev_user FOREIGN KEY (dev_user) REFERENCES users.dev_users(id) NOT VALID;


--
-- TOC entry 3534 (class 2606 OID 16710)
-- Name: refresh_token_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.refresh_token_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3533 (class 2606 OID 16715)
-- Name: session_token_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.session_token_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3536 (class 2606 OID 16720)
-- Name: username_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.username_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3535 (class 2606 OID 16725)
-- Name: password_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.password_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3537 (class 2606 OID 32798)
-- Name: security_questions_settings project_id; Type: FK CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.security_questions_settings
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3532 (class 2606 OID 16522)
-- Name: end_user_settings end_user_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_user_settings
    ADD CONSTRAINT end_user_id FOREIGN KEY (end_user_id) REFERENCES users.end_users(id) ON DELETE CASCADE;


--
-- TOC entry 3538 (class 2606 OID 32803)
-- Name: security_questions end_user_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.security_questions
    ADD CONSTRAINT end_user_id FOREIGN KEY (end_user_id) REFERENCES users.end_users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 3531 (class 2606 OID 16517)
-- Name: end_users project_id; Type: FK CONSTRAINT; Schema: users; Owner: postgres
--

ALTER TABLE ONLY users.end_users
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES projects.projects(id) ON DELETE CASCADE;


-- Completed on 2023-05-14 20:21:30 BST

--
-- PostgreSQL database dump complete
--

